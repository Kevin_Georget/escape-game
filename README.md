les feuilles de style, scripts, images et autres éléments qui ne seront jamais générés dynamiquement doivent être dans le dossier static.
les fichiers HTML doivent être dans le dossier templates.

Liens pour les icones utilisés:
https://ionicons.com/

# Création de l'environement (seulement s'il n'y a pas de dossier env déjà créé)
  - source env.sh

# Lancement de flask (pour lancer le projet)
  - flask run

# Pour recevoir le travail de Killian :
  - git pull Killian

# Pour envoyer le travail à Killian :
  - commit (sur une branche)
  - push (sur la branche)
  - *sur le site* -> faire un merge request de sa branch vers celle de Killian

# Création des tables
  - flask syncdb

# Insertions
  - Commandes disponible en faisant flask
  - Ex: flask loadpers escape/etat_listes_pedagogiques_email_2A_2019-2020.xls
