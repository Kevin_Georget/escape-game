"""
Ce fichier permet de créer les commandes flask
"""
from datetime import datetime, date
import click
import xlrd
import os
from xlwt import *
from .app import APP, DB
from .images import *
from .models import *
from .requests import getPersonneFromEmail, getMaxIdAssociationPersonneGroupe
from sqlalchemy.sql import collate
# from sqlalchemy import exc

@APP.cli.command()
def syncdb():
    '''
    Supression et recréation de toutes les tables de la BD
    '''
    DB.drop_all()
    DB.create_all()

@APP.cli.command()
@click.pass_context
def launch(ctx):
    '''
    Refaire la BD et insérer le loadsample
    '''
    ctx.forward(syncdb)
    ctx.forward(loadsample)

@APP.cli.command()
@click.argument('filename')
def loadpers(filename="etat_listes_pedagogiques_email_2A_2019-2020.xls"):
    '''
    Création des personnes dans la bd à partir du fichier exel
    '''
    # Lecture du fichier excel
    try:
        document = xlrd.open_workbook(filename)
    except ValueError:
        print("Fichier non conforme")
    loadpers_from_exel_file(document)


def loadpers_from_exel_file(document):
    '''
    Création des personnes dans la bd à partir du fichier exel
    '''
    #Récupération de la première feuille et des colonnes/lignes contentant les infos
    feuille_1 = document.sheet_by_index(0)
    # cols = feuille_1.ncols
    rows = feuille_1.nrows

    #Boucle sur chaque ligne pour récupérer les personnes
    cpt_pers = 0
    cpt_groupe = 0
    for row in range(4, rows):
        id = round([feuille_1.cell_value(rowx=row, colx=0)][0])
        email = [feuille_1.cell_value(rowx=row, colx=1)][0]
        #On vérifie que la personne(avec cet id) n'est pas déja dans la bd
        if not Personne.query.get(id) and getPersonneFromEmail(email) is None:
            nom = [feuille_1.cell_value(rowx=row, colx=3)][0]
            groupes = [feuille_1.cell_value(rowx=row, colx=6)][0]

            pers = Personne(id=id,
                            est_prof=email.endswith("@univ-orleans.fr"),
                            prenom=[feuille_1.cell_value(rowx=row, colx=4)][0],
                            nom=nom,
                            email=email)
            #On met le nom de la personne comme mdp et on le transforme en hash
            pers.set_password(nom)
            DB.session.add(pers)
            for groupe in groupes.split(','):
                newGroupe = Groupe.query.filter(Groupe.nom == groupe).all()
                if(groupe):
                    if(not newGroupe):
                        newGroupe = Groupe(nom=groupe)
                        DB.session.add(newGroupe)
                        cpt_groupe =+ 1
                    else:
                        newGroupe = newGroupe[0]
                    association_personne_groupe = AssociationPersonneGroupe(id=getMaxIdAssociationPersonneGroupe()+1 , id_groupe=newGroupe.id, id_personne=pers.id)
                    DB.session.add(association_personne_groupe)
            cpt_pers += 1
    DB.session.commit()
    print(str(cpt_pers) + " personne(s)" + ((" et " + str(cpt_groupe)+" groupes") if cpt_groupe else "") + " ont été ajoutés à la bd")


@APP.cli.command()
def exportetu():
    '''
    Commande d'exportation des personnes dans un fichier exel
    '''
    exportetu()

def exportetu():
    '''
    Exportation des personnes dans un fichier exel (dans le dossier static/exports_xls/etudiants)
    '''
    date = datetime.now()
    #Création du document
    document = Workbook()
    #Ajout d'une feuille
    sheet = document.add_sheet("listes_etudiants")

    #Header de la feuille (date de création + description)
    sheet.write_merge(r1=0, c1=0, r2=0, c2=6,
                      label="Date de création : " + date.strftime("%d/%m/%Y %H:%M:%S"))
    sheet.write_merge(r1=1, c1=0, r2=2, c2=6,
                      label="Ce fichier contient la liste des étudiants à inscrire sur la plateforme Math'Scape\n"+
                            "Si des étudiants sont inclus dans plusieurs groupes, ceux ci sont séparés par une virgule (exemple : Groupe1,Groupe6)")
    #Redimension des colonnes
    sheet.col(0).width = 4000
    sheet.col(1).width = 5000
    sheet.col(2).width = 8000
    sheet.col(3).width = 8000
    sheet.col(4).width = 4000
    sheet.col(5).width = 4000
    sheet.col(6).width = 6000

    #Ajouts des Titres des colonnes
    style = easyxf('font: bold 1; alignment: horizontal center; borders: left thin, right thin, top thin, bottom thin;')
    sheet.write(3, 0, "Numero étudiant", style = style)
    sheet.write_merge(r1=3, c1=1, r2=3, c2=2, label="Email", style=style)
    sheet.write(3, 3, "NOM", style = style)
    sheet.write_merge(r1=3, c1=4, r2=3, c2=5, label="Prénom", style=style)
    sheet.write(3, 6, "Groupe", style = style)

    #Ajouts des étudiants
    etudiants = Personne.query.filter(Personne.est_prof == False).order_by(collate(Personne.nom, 'NOCASE')).all()
    style2 = easyxf("borders: left thin, right thin, top thin, bottom thin;")
    for i in range (len(etudiants)):
        j = i+4
        sheet.write(j, 0, etudiants[i].id, style=easyxf("alignment: horizontal center; borders: left thin, right thin, top thin, bottom thin;"))
        sheet.write_merge(r1=j, c1=1, r2=j, c2=2, label=etudiants[i].email, style=style2)
        sheet.write(j, 3, etudiants[i].nom, style=style2)
        sheet.write_merge(r1=j, c1=4, r2=j, c2=5, label=etudiants[i].prenom, style=style2)
        sheet.write(j, 6, ",".join(groupe.nom for groupe in etudiants[i].groupes), style=style2)

    #Suppression des anciens fichiers et sauvegarde du document
    nomDoc = "exports_xls/etudiants/Math'Scape_liste_etudiants_" + date.strftime("%d_%m_%Y") + '.xls'
    for file in os.listdir('escape/static/exports_xls/etudiants/'):
        if file.endswith(".xls"):
            os.remove("escape/static/exports_xls/etudiants/"+file)
    document.save('escape/static/' + nomDoc)
    return nomDoc


@APP.cli.command()
@click.argument('filename')
def loadenigmes(filename):
    '''
    Création des enigmes dans la bd à partir du fichier exel
    '''
    # Lecture du fichier excel
    try:
        document = xlrd.open_workbook(filename)
    except ValueError:
        print("Fichier non conforme")
    print(loadenigmes(document))


def loadenigmes(document):
    '''
    Création des personnes dans la bd à partir du fichier exel
    '''
    #Récupération de la première feuille et des colonnes/lignes contentant les infos
    feuille_1 = document.sheet_by_index(0)
    cols = feuille_1.ncols
    rows = feuille_1.nrows

    #Boucle sur chaque ligne pour récupérer les enigmes
    cpt_enigmes = 0
    cpt_indices = 0
    for row in range(4, rows):
        intitule = [feuille_1.cell_value(rowx=row, colx=0)][0]
        if (not EnigmeSpeciale.query.filter(EnigmeSpeciale.intitule == intitule).all()):
            solution = [feuille_1.cell_value(rowx=row, colx=1)][0]
            nb_points = [feuille_1.cell_value(rowx=row, colx=2)][0]
            nom_diff = [feuille_1.cell_value(rowx=row, colx=3)][0]

            #Ajout de la difficulté et du dans la bd si ils n'y sont pas déja
            difficulte = Difficulte.query.filter(Difficulte.nom == nom_diff).all()
            if (not difficulte):
                difficulte = Difficile(nom = nom_diff)
                DB.session.add(difficulte)
            else:
                difficulte = difficulte[0]
            nom_theme = [feuille_1.cell_value(rowx=row, colx=4)][0]
            theme = Theme.query.filter(Theme.nom == nom_theme).all()
            if (not theme):
                theme = Theme(nom=nom_theme)
                DB.session.add(theme)
            else:
                theme = theme[0]
            DB.session.commit()
            enigme_speciale = EnigmeSpeciale(intitule=intitule, solution=solution, nb_points=nb_points, id_difficulte=difficulte.id, id_theme=theme.id, nb_points_groupe_courant=nb_points, est_resolu_groupe_courant=False)
            DB.session.add(enigme_speciale)
            indices = []
            DB.session.commit()
            #Indices de l'enigme
            for i in range(5, cols):
                contenu = [feuille_1.cell_value(rowx=row, colx=i)][0]
                if contenu:
                    indice_speciale = IndiceSpeciale(contenu=contenu, id_enigme_speciale=enigme_speciale.id, est_trouve=False)
                    DB.session.add(indice_speciale)
                    cpt_indices += 1
            cpt_enigmes += 1
    DB.session.commit()
    return str(cpt_enigmes) + " enigme" + ("s" if cpt_enigmes != 1 else "") + ((" et " + str(cpt_indices)+" indice(s) ") if cpt_indices else " ") + "ont été ajoutés à la bd"


@APP.cli.command()
def exportenigmes():
    '''
    Commande d'exportation des personnes dans un fichier exel
    '''
    exportenigmes()

def exportenigmes():
    '''
    Exportation des enigmes dans un fichier exel (dans le dossier static/exports_xls/enigmes)
    '''
    date = datetime.now()
    #Création du document
    document = Workbook()
    #Ajout d'une feuille
    sheet = document.add_sheet("listes_enigmes")

    #Header de la feuille (date de création + description)
    sheet.write_merge(r1=0, c1=0, r2=0, c2=5, label="Date de création : " + date.strftime("%d/%m/%Y %H:%M:%S"))
    sheet.write_merge(r1=1, c1=0, r2=2, c2=5, label="Ce fichier contient la liste des énigmes de la plateforme Math'Scape")

    #Redimension des colonnes
    sheet.col(0).width = 10000
    sheet.col(1).width = 5000
    sheet.col(2).width = 5000
    sheet.col(3).width = 5000
    sheet.col(4).width = 5000
    for i in range(5, 256):
        sheet.col(i).width = 10000
    #Ajouts des Titres des colonnes
    style = easyxf('font: bold 1; alignment: horizontal center; borders: left thin, right thin, top thin, bottom thin;')
    sheet.write(3, 0, "Intitulé de l'enigme", style)
    sheet.write(3, 1, "Solution", style)
    sheet.write(3, 2, "Nombre de points", style)
    sheet.write(3, 3, "Difficulté", style)
    sheet.write(3, 4, "Thème", style)
    sheet.write(3, 5, "Indices", style)

    #Ajouts des enigmes
    enigmes_speciales = EnigmeSpeciale.query.all()
    style2 = easyxf("borders: left thin, right thin, top thin, bottom thin;")
    for i in range (len(enigmes_speciales)):
        j = i+4
        enigme_speciale = enigmes_speciales[i]
        sheet.write(j, 0, enigme_speciale.intitule, style=style2)
        sheet.write(j, 1, enigme_speciale.solution, style=style2)
        sheet.write(j, 2, enigme_speciale.nb_points, style=style2)
        if enigme_speciale.difficulte:
            sheet.write(j, 3, enigme_speciale.difficulte.nom, style=style2)
        if enigme_speciale.theme:
            sheet.write(j, 4, enigme_speciale.theme.nom, style=style2)
        #Ajouts des indices de l'enigme
        print(enigme_speciale.indices_speciales)
        for i in range(len(enigme_speciale.indices_speciales)):
            print(enigme_speciale.indices_speciales[i].contenu)
            sheet.write(j, 5+i, enigme_speciale.indices_speciales[i].contenu, style=style2)

    #Suppression des anciens fichiers et sauvegarde du document
    nomDoc = "exports_xls/enigmes/Math'Scape_liste_enigmes_" + date.strftime("%d_%m_%Y") + '.xls'
    for file in os.listdir('escape/static/exports_xls/enigmes/'):
        if file.endswith(".xls"):
            os.remove("escape/static/exports_xls/enigmes/"+file)
    document.save('escape/static/' + nomDoc)

    return nomDoc

@APP.cli.command()
def loadsample():
    '''
    Créé des objets pour chaque classe de la bd
    '''
    association_groupe_indice = AssociationGroupeIndice(id=1, id_groupe=1, id_indice=1)
    DB.session.add(association_groupe_indice)

    association_groupe_salle = AssociationGroupeSalle(id=1, est_decouverte=False, id_groupe=1, id_salle=1)
    DB.session.add(association_groupe_salle)
    association_groupe_salle2 = AssociationGroupeSalle(id=2, est_decouverte=False, id_groupe=1, id_salle=2)
    DB.session.add(association_groupe_salle2)

    association_groupe_zone = AssociationGroupeZone(id_zone=1, id_groupe=1)
    DB.session.add(association_groupe_zone)
    association_groupe_zone2 = AssociationGroupeZone(id_zone=2, id_groupe=1)
    DB.session.add(association_groupe_zone2)

    association_personne_groupe = AssociationPersonneGroupe(id_personne=2180205, id_groupe=1)
    association_personne_groupe2 = AssociationPersonneGroupe(id_personne=2184445, id_groupe=2)
    association_personne_groupe3 = AssociationPersonneGroupe(id_personne=2189999, id_groupe=3)
    DB.session.add(association_personne_groupe)
    DB.session.add(association_personne_groupe2)
    DB.session.add(association_personne_groupe3)

    association_indice_enigme = AssociationIndiceEnigme(id=1, id_indice=1, id_enigme=1)
    DB.session.add(association_indice_enigme)

    association_enigme_groupe = AssociationEnigmeGroupe(id=1, id_enigme=1, id_groupe=1)
    DB.session.add(association_enigme_groupe)

    association_modele_enigme_modele_salle = AssociationModeleEnigmeModeleSalle(id=1, id_modele_enigme=1, id_modele_salle=1)
    DB.session.add(association_modele_enigme_modele_salle)

    association_modele_indice_modele_salle = AssociationModeleIndiceModeleSalle(id=1, id_modele_indice=1, id_modele_salle=1)
    DB.session.add(association_modele_indice_modele_salle)



    enigme_speciale = EnigmeSpeciale(id=1, intitule="intitule de l'enigme spe", solution="42", nb_points=10, id_difficulte=1, id_theme=1, nb_points_groupe_courant=10, est_resolu_groupe_courant=False)
    DB.session.add(enigme_speciale)

    indice_speciale = IndiceSpeciale(id=1, contenu="Trouve par toi même Polytechnicien", id_enigme_speciale=1, est_trouve=False)
    DB.session.add(indice_speciale)
    indice_speciale2 = IndiceSpeciale(id=2, contenu="La réponse se situe entre 41 et 43", id_enigme_speciale=1, est_trouve=False)
    DB.session.add(indice_speciale2)



    personne = Personne(id=2184445, est_prof=True, prenom="Killian", nom="Denechere", email="killian@etu-orleans.fr")
    personne.set_password('123')
    DB.session.add(personne)

    personne2 = Personne(id=2180205, est_prof=False, prenom="Thibaud", nom="Duprez", email="thibaud.duprez@etu-orleans.fr")
    personne2.set_password('1234')
    DB.session.add(personne2)

    personne3 = Personne(id=2189999, est_prof=False, prenom="Toto", nom="Test", email="g@stro.nomi")
    personne3.set_password('123')
    DB.session.add(personne3)

    groupe = Groupe(id=1, nom="2A22")
    groupe2 = Groupe(id=2, nom="2A21")
    groupe3 = Groupe(id=3, nom="2A1")
    DB.session.add(groupe)
    DB.session.add(groupe2)
    DB.session.add(groupe3)

    modele_zone = ModeleZone(id=1, pos_x=0.367, pos_y=0.202, hauteur=1.11, largeur=0.17, id_modele_salle=1)
    DB.session.add(modele_zone)
    modele_zone2 = ModeleZone(id=2, pos_x=0.367, pos_y=0.202, hauteur=1.11, largeur=0.17, id_modele_salle=2)
    DB.session.add(modele_zone2)

    zone = Zone(id=1, contient_indice=True, id_salle=1, id_modele_zone=1)
    DB.session.add(zone)
    zone2 = Zone(id=2, contient_indice=True, id_salle=2, id_modele_zone=2)
    DB.session.add(zone2)

    modele_escape = ModeleEscape(id=1, nom="Le manoir", desc="Trouvez qui est responsable du meurtre de Mr Lenoir (non ce n'est pas une copie de Cluedo !)", id_personne=2184445)
    DB.session.add(modele_escape)

    escape_game = EscapeGame(id=1, date_debut=datetime.now(), date_fin=datetime(2020, 5, 1, 12, 0, 0), id_modele_escape=1)
    DB.session.add(escape_game)

    image = Image(id=1, image=IMAGE1)
    DB.session.add(image)

    image2 = Image(id=2, image=IMAGE2)
    DB.session.add(image2)

    image3 = Image(id=3, image=IMAGE3)
    DB.session.add(image3)

    modele_enigme = ModeleEnigme(id=1, intitule="Combien font 2+2 ?", solution="4", nb_points=5, id_image=2)
    DB.session.add(modele_enigme)

    enigme = Enigme(id=1, id_modele_enigme=1, est_resolu=False, nb_points=modele_enigme.nb_points)
    DB.session.add(enigme)

    modele_salle = ModeleSalle(id=1, desc="Vous vous situez devant le manoir, comment entrer ?", est_libre=True, est_personnalisee=True, ordre_salle=1, id_image=1, id_modele_escape=1, utilise_enigme_spe=0)
    DB.session.add(modele_salle)
    modele_salle2 = ModeleSalle(id=2, desc="", est_libre=True, est_personnalisee=True, ordre_salle=2, id_image=None, id_modele_escape=1, utilise_enigme_spe=0)
    DB.session.add(modele_salle2)

    salle = Salle(id=1, id_escape_game=1, id_modele_salle=1, id_enigme=1)
    DB.session.add(salle)
    salle2 = Salle(id=2, id_escape_game=1, id_modele_salle=2, id_enigme=None)
    DB.session.add(salle2)

    difficulte = Difficulte(id=1, nom="Facile")
    difficulte2 = Difficulte(id=2, nom="Moyen")
    difficulte3 = Difficulte(id=3, nom="Difficile")
    DB.session.add(difficulte)
    DB.session.add(difficulte2)
    DB.session.add(difficulte3)

    theme = Theme(id=1, nom="Mathématique")
    DB.session.add(theme)

    modele_indice = ModeleIndice(id=1, contenu="Un couteau, c'est peut être l'arme du crime", id_modele_enigme=1, id_image=3)
    DB.session.add(modele_indice)

    indice = Indice(id=1, id_zone=1, id_modele_indice=1, est_trouve=False)
    DB.session.add(indice)

    # jeu_variable = JeuVariable(id=1, id_modele_enigme=1)
    # DB.session.add(jeu_variable)

    # variable = Variable(id=1, nom="variable1", valeur=2)
    # DB.session.add(variable)


    DB.session.commit()


@APP.cli.command()
def test():
    """
    Tests de requetes
    """
    # print("\nTest des requetes du fichier requests.py :\n")
    # test_all_request()
    # print("\nAutre test : \n")
    print(AssociationGroupeZone.query.first().zone)
    print(Groupe.query.first().personnes)
    print(Salle.query.first().escape_game)
    print(Image.query.first().modele_salles)
    print(Zone.query.first().salle)
    print(ModeleEnigme.query.first().difficulte)
    print(ModeleEnigme.query.first().theme)
    print(ModeleEscape.query.first().personne)
    print(EnigmeSpeciale.query.first())
    print(EnigmeSpeciale.query.first().indices_speciales)
    print(AssociationZoneIndiceSpeciale.query.first().zone)
    print(Indice.query.first().enigmes)
    print(Zone.query.first().indices)
    print(Enigme.query.first().groupes)
    print(Salle.query.first().indice)
    print(ModeleEnigme.query.first().modele_salles)
    print(EscapeGame.query.first().modele_escape)
    print(Salle.query.first().modele_salle)
    print(ModeleSalle.query.first().image)
    print(Enigme.query.first().modele_enigme)
    print(ModeleSalle.query.first().modele_escape)
    print(Indice.query.first().groupes)
    print(Enigme.query.first().associations_enigme_groupe)
    print(Indice.query.first().modele_indice)
    print(Groupe.query.join(Groupe.salles).join(Salle.escape_game).filter(EscapeGame.id == 1).all())
    print(Personne.query.join(Personne.groupes).join(Groupe.salles).join(Salle.escape_game).filter(EscapeGame.id == 1).all())
    print(AssociationModeleIndiceModeleSalle.query.first().modele_indice)
    # print(JeuVariable.query.first().modele_enigme)
    # print(AssociationJeuVariableVariable.query.first().variable)
    print(Zone.query.first().modele_zone.modele_salle)
