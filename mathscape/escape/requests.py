from .models import *
from .app import login_manager, DB, mkpath
from sqlalchemy import func
from datetime import datetime
import random
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.sql import collate

############################ ESCAPES ############################
def getListeEscapeParProf(idProf):
    """
    Retourne la liste des escapes games (modèles) appartenant au prof pointé par idProf
    """
    return Personne.query.get(idProf).modele_escapes

def getEscapeFromGroupeOuverts(idGroupe):
    """
    Retourne la liste des EscapeGame encore actifs auxquels participent le groupe idGroupe
    """
    res = []
    listeAssociations = AssociationGroupeSalle.query.filter_by(id_groupe = idGroupe).all()
    if(listeAssociations != []):
        for association in listeAssociations:
            salle = Salle.query.filter_by(id = association.id_salle).first()
            if(salle != None):
                escape = EscapeGame.query.filter_by(id = salle.id).first()
                if(escape != None):
                    if(escape.date_fin >= datetime.today()):
                        if(escape.salles):
                            ajouter = False
                            for salle in escape.salles:
                                if(salle.enigme or salle.enigmes_speciales or salle.modele_salle.est_libre): #Il faut que le modele de salle soit déclaré comme libre ou qu'il y ait soit une enigme personnalisé, soit une énigme spéciale
                                    ajouter = True
                                    break;
                            if(ajouter):
                                res.append(escape)
    return res

def getEscapeFromGroupeFini(idGroupe):
    """
    Retourne la liste des EscapeGame(instances) fini auxquels participe le groupe idGroupe
    """
    res = []
    listeAssociations = AssociationGroupeSalle.query.filter_by(id_groupe = idGroupe).all()
    if(listeAssociations != []):
        for association in listeAssociations:
            salle = Salle.query.filter_by(id = association.id_salle).first()
            if(salle != None):
                escape = EscapeGame.query.filter_by(id = salle.id).first()
                if(escape != None):
                    if escape.date_fin < datetime.today() :
                        res.append(escape)
    return res

def getListeEscapeDeEleve(idEleve):
    """
    Retourne la liste des escapes auxquels l'élève identifié par idEleve parricipe
    """
    return EscapeGame.join(Salle).join(Groupe).join(Personne).query.filter_by(Personne.id == idEleve).all()

def getListeEscape():
    """
    Retourne la liste de escape games (modèles) de la plateforme
    """
    return ModeleEscape.query.all()

def getInstanceEscapeFromId(idInstance):
    """
    Retourne l'instance d'escape identifiée par idInstance
    """
    return EscapeGame.query.get_or_404(idInstance)

def getModeleEscapeFromId(idEscape):
    """
    Renvoie l'escape (le modèle) dont l'identifiant est idEscape
    """
    return ModeleEscape.query.get_or_404(idEscape)

def getListeInstancesFromEscapeId(escapeId):
    """
    Retourne la liste des instances d'un modèle d'escape game
    """
    return EscapeGame.query.filter_by(id_modele_escape = escapeId).all()

def getListeInstancesAvecGroupesFromEscapeId(escapeId):
    """
    Retourne la liste des instances d'un escape game (modèle) avec les groupes y participant ainsi que la composition de ceux-ci
    """
    res = []
    modeleSalle = ModeleSalle.query.filter_by(id_modele_escape = escapeId).first() #premier modèle de salle du modèle d'escape
    if(modeleSalle != None):
        listeInstances = EscapeGame.query.filter_by(id_modele_escape = escapeId).all()
        if(listeInstances != []):
            for instance in listeInstances: #toute les instances du modèle d'escape
                instanceSalle = Salle.query.filter_by(id_modele_salle = modeleSalle.id).filter_by(id_escape_game=instance.id).first() #la première instance de ce modèle de salle
                if(instanceSalle != None):
                    groupe = AssociationGroupeSalle.query.filter_by(id_salle = instanceSalle.id).first()# le groupe associé à cette instance
                    res.append((instance, groupe.groupe, getMembresGroupeFromId(groupe.groupe.id)))
    return res

def getInstanceFromEscapeIdAndGroupeId(escapeId,groupeId):
    """
    Retourne l'instance d'un escape game (modèle) dont le groupe participe
    """
    instances = getListeInstancesAvecGroupesFromEscapeId(escapeId)
    for instance in instances:
        groupe = instance[1]
        if (groupe.id==groupeId):
            return instance
    return None

def getListeGroupesInEscape(escapeId):
    """
    Retourne la liste des groupes d'un escape(modèle) identifié par escapeId
    """
    res = []
    modeleSalle = ModeleSalle.query.filter_by(id_modele_escape = escapeId).first() #premier modèle de salle du modèle d'escape
    if(modeleSalle != None):
        for instance in EscapeGame.query.filter_by(id_modele_escape = escapeId).all(): #toute les instances du modèle d'escape
            instanceSalle = Salle.query.filter_by(id_modele_salle = modeleSalle.id).filter_by(id_escape_game=instance.id).first() #la première instance de ce modèle de salle
            if(instanceSalle != None):
                groupe = AssociationGroupeSalle.query.filter_by(id_salle = instanceSalle.id).first()# le groupe associé à cette instance
                res.append(groupe.groupe)
    return res

def getListeSallesFromEscapeId(escapeId):
    """
    Retourne la liste des modèles de salles qui composent le scénario de l'escape (le modèle)
    """
    return ModeleSalle.query.filter_by(id_modele_escape = escapeId).order_by(ModeleSalle.ordre_salle.asc()).all()

def getListeGroupesAjoutableModeleEscapeFromId(escapeId):
    """
    Retourne la liste des groupes qu'il est possible d'ajouter à un modèle escape (pour créer une instance donc)
    """
    listeGroupes = getListeGroupes()
    listeGroupesAlreadyInEscape = getListeGroupesInEscape(escapeId)
    res = []
    for groupe in listeGroupes:
        if groupe not in listeGroupesAlreadyInEscape:
            res.append(groupe)
    return res

def getMaxIdInstancesEscape():
    """
    Retourne l'identifiant maximum enregistré pour les instances d'escapes games
    """
    res = DB.session.query(DB.func.max(EscapeGame.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdModeleEscape():
    """
    Retourne l'identifiant maximum enregistré pour les modeles d'escapes games
    """
    res = DB.session.query(DB.func.max(ModeleEscape.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdTheme():
    """
    Retourne l'identifiant maximum enregistré pour les thèmes
    """
    res = DB.session.query(DB.func.max(Theme.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getNoteFromEscape(idEscape, sur20=False):
    """
    Retourne la note actuelle, maximale obtensible et totale d'un escape game (instance) identifé par idEscape.
    """
    escape = getInstanceEscapeFromId(idEscape)
    total = 0
    noteActuelle = 0
    maxPossible = 0
    if(escape.salles):
        for salle in escape.salles:
            if(salle.enigme != None and salle.enigmes_speciales == []): #Si on a seulement une énigme personnalisé
                print("Calcul en fonction de l'énigme personnalisée")
                res = calculNoteEnigmePersonnalisee(salle, total, noteActuelle, maxPossible)
                total += res[0]
                noteActuelle += res[1]
                maxPossible += res[2]
            elif(salle.enigmes_speciales != [] and salle.enigme == None): #si on a seulement une enigme speciale
                print("Calcul en fonction de l'énigme spéciale")
                res = calculNoteEnigmeSpeciale(salle, total, noteActuelle, maxPossible)
                total += res[0]
                noteActuelle += res[1]
                maxPossible += res[2]
            elif(salle.enigmes_speciales != [] and salle.enigme != None): #si on a les 2
                print("Vérif du mode prioritaire")
                if(salle.modele_salle.utilise_enigme_spe == 1):
                    print("mode trouvé : enigme pré-enregistrée")
                    res = calculNoteEnigmeSpeciale(salle, total, noteActuelle, maxPossible)
                    total += res[0]
                    noteActuelle += res[1]
                    maxPossible += res[2]
                else:
                    print("mode trouvé : enigme personnalisée")
                    res = calculNoteEnigmePersonnalisee(salle, total, noteActuelle, maxPossible)
                    total += res[0]
                    noteActuelle += res[1]
                    maxPossible += res[2]
    if(sur20):
        if(total != 0):
            return [round((noteActuelle*20)/total, 2), 20, round((maxPossible*20)/total, 2)]
        return [0, 0, 0]
    return [noteActuelle, total, maxPossible]

def calculNoteEnigmePersonnalisee(salle, total, noteActuelle, maxPossible):
    """
    Ne pas utiliser dette fonction seule : elle est juste appelée plusieurs fois depuis une autre fonction pour les notes
    """
    t, m, n = 0, 0, 0
    t = total + salle.enigme.modele_enigme.nb_points
    m = maxPossible + salle.enigme.nb_points
    if(salle.enigme.est_resolu == True):
        n = noteActuelle + salle.enigme.nb_points
    return (t, n, m)

def calculNoteEnigmeSpeciale(salle, total, noteActuelle, maxPossible):
    """
    Ne pas utiliser dette fonction seule : elle est juste appelée plusieurs fois depuis une autre fonction pour les notes
    """
    t, m, n = 0, 0, 0
    t = total + salle.enigmes_speciales[0].nb_points
    m = maxPossible + salle.enigmes_speciales[0].nb_points_groupe_courant
    if(salle.enigmes_speciales[0].est_resolu_groupe_courant == True):
        n = noteActuelle + salle.enigmes_speciales[0].nb_points_groupe_courant
    return (t, n, m)

# ----------------------------------------------

def modifierModeleEscape(escape, nom, description):
    """
    Modifie les informations d'un escape game (le modèle)
    """
    escape.nom = nom
    escape.desc = description
    DB.session.commit()

def modifierDateFinInstanceEscape(idInstance, valeur):
    """
    Modifie la date de fin d'une instance d'escape game
    """
    getInstanceEscapeFromId(idInstance).date_fin = datetime.strptime(valeur, '%Y-%m-%d')
    DB.session.commit()

def modifierDateDebutInstanceEscape(idInstance, valeur):
    """
    Modifie la date de début d'une instance d'escape game
    """
    getInstanceEscapeFromId(idInstance).date_debut = datetime.strptime(valeur, '%Y-%m-%d')
    DB.session.commit()

# ----------------------------------------------

def supprimerInstanceEscapeFromId(idInstance):
    """
    Supprimer l'instance d'escape identifée par idInstance
    Supprime aussi les instances de salle associées
    """
    instance = getInstanceEscapeFromId(idInstance)

    # Salles et associations groupes salles
    for salle in instance.salles:
        if(salle.zones):
            for zone in salle.zones:
                # suppression de la zone part1
                DB.session.delete(AssociationGroupeZone.query.filter_by(id_zone = zone.id).first())

                if(zone.contient_indice):
                    for indice in zone.indices:
                        # suppression de l'indice part1
                        DB.session.delete(AssociationGroupeIndice.query.filter_by(id_indice = indice.id).first())

                        associationIndiceEnigme = AssociationIndiceEnigme.query.filter_by(id_indice = indice.id).first()
                        if(associationIndiceEnigme):
                            enigme = Enigme.query.filter_by(id=associationIndiceEnigme.id_enigme).first()

                            # suppression de l'indice part2
                            DB.session.delete(associationIndiceEnigme)
                            if(enigme != None):
                                # suppression de l'enigme
                                DB.session.delete(AssociationEnigmeGroupe.query.filter_by(id_enigme = enigme.id).first())
                                DB.session.delete(enigme)

                        # suppression de l'indice part3
                        DB.session.delete(indice)

                # suppression de la zone part2
                if(zone.indices_speciales):
                    DB.session.delete(AssociationZoneIndiceSpeciale.query.filter(AssociationZoneIndiceSpeciale.id_indice_speciale == zone.indices_speciales[0].id, AssociationZoneIndiceSpeciale.id_zone == zone.id).first())
                DB.session.delete(zone)

        # suppression de la salle part1
        if(salle.enigmes_speciales):
            enigmeSpe = salle.enigmes_speciales[0]
            enigmeSpe.nb_points_groupe_courant = enigmeSpe.nb_points
            enigmeSpe.est_resolu_groupe_courant = False
            DB.session.delete(AssociationEnigmeSpecialeSalle.query.filter(AssociationEnigmeSpecialeSalle.id_salle==salle.id, AssociationEnigmeSpecialeSalle.id_enigme_speciale==enigmeSpe.id).first())
            DB.session.delete(AssociationGroupeEnigmeSpeciale.query.filter(AssociationGroupeEnigmeSpeciale.id_enigme_speciale==enigmeSpe.id, AssociationGroupeEnigmeSpeciale.id_groupe==salle.groupes[0].id).first())
            if(enigmeSpe.indices_speciales):
                for indiceSpe in enigmeSpe.indices_speciales:
                    association = AssociationGroupeIndiceSpeciale.query.filter(AssociationGroupeIndiceSpeciale.id_indice_speciale==indiceSpe.id, AssociationGroupeIndiceSpeciale.id_groupe==salle.groupes[0].id).first()
                    if(association):
                        DB.session.delete(association)
                    DB.session.commit()

        # supprimer salle part2
        DB.session.delete(AssociationGroupeSalle.query.filter_by(id_salle = salle.id).first())

        DB.session.delete(salle)

    # suppression de l'escape
    DB.session.delete(instance)
    DB.session.commit()

def supprimerModeleEscape(idModeleEscape):
    """
    Supprime le modele d'escape identifié par idModeleEscape
    """
    modele = getModeleEscapeFromId(idModeleEscape)
    for instance in getListeInstancesFromEscapeId(idModeleEscape):
        supprimerInstanceEscapeFromId(instance.id)
    for modeleEnigme in getListeModeleEnigmeFromModeleEscape(idModeleEscape):
        supprimerModeleEnigme(modeleEnigme.id)
    for modeleSalle in getListeSallesFromEscapeId(idModeleEscape):
        for modeleIndice in getListeModeleIndiceFromSalle(modeleSalle.id):
            supprimerModeleIndice(modeleIndice.id)
        supprimerModeleSalle(modeleSalle.id, idModeleEscape)
    DB.session.delete(modele)
    DB.session.commit()

def supprimerTheme(idTheme):
    """
    Supprime le thème identifé par idTheme
    """
    DB.session.delete(Theme.query.filter_by(id=idTheme).first())
    DB.session.commit()

# ----------------------------------------------

def ajouterVersionPourEscape(idEscape, idGroupe):
    """
    Créé une instance pour l'escape game idEscape liée avec le groupe identifié par idGroupe
    """
    instanceEscape = EscapeGame(id=getMaxIdInstancesEscape()+1, date_debut=datetime.now(), date_fin=datetime.now(), id_modele_escape=idEscape) #Création de l'instance
    DB.session.add(instanceEscape)

    for modeleSalle in getListeSallesFromEscapeId(idEscape): #Pour toutes les salles du modèle d'escape

        # CREATION INSTANCE
        instanceSalle = Salle(id=getMaxIdInstancesSalle()+1, id_escape_game=instanceEscape.id, id_enigme=None, id_modele_salle=modeleSalle.id) #Création d'une instance de cette salle
        DB.session.add(instanceSalle)

        # ASSOCIATION AU GROUPE
        associationGroupeSalle = AssociationGroupeSalle(id=getMaxIdAssociationGroupeSalle()+1, est_decouverte=False, id_groupe=idGroupe, id_salle=instanceSalle.id) #Association du groupe à cette instance de salle
        DB.session.add(associationGroupeSalle)

        # INDICES
        listeModelesIndicesPlacablesDansZone = []
        if(modeleSalle.modele_indices != []):
            for modeleIndice in modeleSalle.modele_indices:
                listeModelesIndicesPlacablesDansZone.append(modeleIndice)

        # ENIGME
        if(modeleSalle.modele_enigmes):
            enigme = Enigme(id=getMaxIdEnigme()+1, id_modele_enigme=modeleSalle.modele_enigmes[0].id, est_resolu=False, nb_points=modeleSalle.modele_enigmes[0].nb_points)
            DB.session.add(enigme)
            association_enigme_groupe = AssociationEnigmeGroupe(id=getMaxIdAssociationEnigmeGroupe()+1, id_enigme=enigme.id, id_groupe=idGroupe)
            DB.session.add(association_enigme_groupe)
            instanceSalle.id_enigme = enigme.id

        # CRÉATION DES ZONES ET REPARTITION INDICES
        listeZones = getListeZonesFromModeleSalle(modeleSalle.id)
        while(listeZones != []):
            if(listeModelesIndicesPlacablesDansZone != []):
                modeleIndice = random.choice(listeModelesIndicesPlacablesDansZone)
                modeleZone = random.choice(listeZones)

                zone = Zone(id=getMaxIdZone()+1, id_salle=instanceSalle.id, id_modele_zone=modeleZone.id, contient_indice=1)
                DB.session.add(zone)

                indice = Indice(id=getMaxIdInstancesIndices()+1, id_zone=zone.id, id_modele_indice=modeleIndice.id, est_trouve=False)
                DB.session.add(indice)

                association_groupe_indice = AssociationGroupeIndice(id=getMaxIdAssociationGroupeIndice()+1, id_groupe=idGroupe, id_indice=indice.id)
                DB.session.add(association_groupe_indice)

                if(modeleSalle.modele_enigmes):
                    association_indice_enigme = AssociationIndiceEnigme(id=getMaxIdAssociationIndiceEnigme()+1,id_indice=indice.id, id_enigme=enigme.id)
                    DB.session.add(association_indice_enigme)

                association_groupe_zone = AssociationGroupeZone(id=getMaxIdAssociationGroupeZone()+1,id_zone=zone.id, id_groupe=idGroupe)
                DB.session.add(association_groupe_zone)

                listeModelesIndicesPlacablesDansZone.remove(modeleIndice)
                listeZones.remove(modeleZone)
            else:
                for modeleZone in listeZones:
                    zone = Zone(id=getMaxIdZone()+1, id_salle=instanceSalle.id, id_modele_zone=modeleZone.id, contient_indice=0)
                    DB.session.add(zone)

                    association_groupe_zone = AssociationGroupeZone(id_zone=zone.id, id_groupe=idGroupe)
                    DB.session.add(association_groupe_zone)

                    listeZones.remove(modeleZone)
    DB.session.commit()

def nouveauModeleEscape(nom, desc, idPers):
    """
    Créé un nouveau modèle d'escape game avec les informations âssées en paramètres
    """
    modele = ModeleEscape(id=getMaxIdModeleEscape()+1, nom=nom, desc=desc, id_personne=idPers)
    DB.session.add(modele)
    DB.session.commit()

############################ ETUDIANTS / PERSONNES ############################
def get_all_etudiants():
    """
    Retourne la liste de tous les étudiants de la plateforme (personnes non prof)
    """
    return Personne.query.filter(Personne.est_prof == False).order_by(collate(Personne.nom, 'NOCASE')).all()



def getPersonneFromId(idPersonne):
    """
    Retourne la personne identifiée par idPersonne
    """
    return Personne.query.get(idPersonne)

def getAllPersonnes():
    """
    Retourne la liste de toutes les personnes de la plateforme
    """
    return Personne.query.all()

def getPersonneFromEmail(email):
    """
    Retourne la personne identifié par l'email
    """
    try:
        return Personne.query.filter(Personne.email == email).one()
    except NoResultFound:
        return None

# ----------------------------------------------

def modifierPersonne(personne, nom, prenom, mail, image = None):
    """
    Modifie les informations enregistrées sur une personne (son nom, prenom et son mail) -> pas son numéro
    """
    personne.nom = nom
    personne.prenom = prenom
    personne.email = mail
    if(image != "" ):
        if(personne.image):
            personne.image.image = image
        else:
            img = Image(id=getMaxIdImage()+1, image=image)
            DB.session.add(img)
            personne.id_image = img.id
    DB.session.commit()

def change_password(id, password):
    etu = getPersonneFromId(id)
    etu.set_password(str(password))
    DB.session.commit()

# ----------------------------------------------

def creerEtudiant(identifiant, nom, prenom, mail, image=None):
    """
    Créé un nouvel étudiant dans la base de données et met son numéro d'étudiant en tant que mot de passe
    """
    if image:
        img = Image(id=getMaxIdImage()+1, image=image)
        DB.session.add(img)
        etudiant = Personne(id=identifiant, est_prof=False, prenom=prenom, nom=nom, email=mail, id_image=img.id)
    else:
        etudiant = Personne(id=identifiant, est_prof=False, prenom=prenom, nom=nom, email=mail)
    DB.session.add(etudiant)
    DB.session.commit()

# ----------------------------------------------

def supprimerEtudiant(idEtudiant):
    """
    Supprimer l'étudiant identifié par idEtudiant
    """
    etudiant = Personne.query.filter(Personne.id == idEtudiant)
    etudiant.delete()
    DB.session.commit()

def supprimerTousEtudiants():
    """
    Supprime tous les étudiants enregistrés
    """
    AssociationPersonneGroupe.query.delete()
    Personne.query.filter(Personne.est_prof == False).delete()
    DB.session.commit()

############################ GROUPES ############################
def getMembresGroupeFromId(idGroupe):
    """
    Retourne les personnes composant le groupe identifié par idGroupe
    """
    res = []
    for personne in AssociationPersonneGroupe.query.filter_by(id_groupe = idGroupe).all():
        res.append(Personne.query.filter_by(id = personne.id_personne).first())
    return res

def getGroupeFromInstanceEscape(idInstance):
    """
    Retourne le groupe associé à l'instance d'escape identifée par idInstance
    """
    return getInstanceEscapeFromId(idInstance).salles[0].groupes[0]

def getListeGroupes():
    """
    Retourne la liste de tous les groupes de la plateforme
    """
    return Groupe.query.all()

def getListeGroupesAvecEleves():
    """
    Retourne la liste de tous les groupes de la plateforme avec les élèves qui les composes
    """
    res = []
    for groupe in getListeGroupes():
        listePersonnesDansGroupe = []
        for association in AssociationPersonneGroupe.query.filter_by(id_groupe = groupe.id).all():
            listePersonnesDansGroupe.append(association.personne)
        listeParticipations = []
        for modeleEscape in getListeEscape():
            if groupe in getListeGroupesInEscape(modeleEscape.id):
                 listeParticipations.append(modeleEscape.nom)
        res.append((groupe, listePersonnesDansGroupe, listeParticipations))
    return res

def getGroupeFromId(idGroupe):
    """
    Retourne le groupe identifié par idGroupe
    """
    return Groupe.query.filter_by(id=idGroupe).first()

def getGroupeIdFromIdPers(idPers):
    """
    Retourne la liste des groupe.id auxquels appartient l'étudiant idPers
    """
    res = []
    for association in AssociationPersonneGroupe.query.filter_by(id_personne = idPers).all():
        res.append(association.groupe.id)
    return res

def getEscapeFromGroupe(idGroupe):
    """
    Retourne la liste des escapes auxquels prticipe un groupe identifé par idGroupe
    """
    res = []
    for association in AssociationGroupeSalle.query.filter_by(id_groupe = idGroupe).all():
        salle = Salle.query.filter_by(id = association.id_salle).first()
        if(salle.escape_game not in res):
            res.append(salle.escape_game)
    return res

def getListeElevesDansAucunGroupe():
    """
    Retourne la liste des élèves de la promo qui ne sont dans aucun groupe
    """
    res = []
    promo = get_all_etudiants()
    for etudiant in promo:
        association = AssociationPersonneGroupe.query.filter_by(id_personne=etudiant.id).first() #Il peut y en avoir plus que 1 mais du moment qu'il est dans dans un groupe, ça suffit
        if(not association):
            res.append(etudiant)
    return res

def getMaxIdGroupe():
    """
    Retourne l'identifiant maximum enregistré pour les groupes
    """
    res = DB.session.query(DB.func.max(Groupe.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdAssociationPersonneGroupe():
    """
    Retourne l'identifiant maximum enregistré pour l'AssociationPersonneGroupe
    """
    res = DB.session.query(DB.func.max(AssociationPersonneGroupe.id).label("max")).one().max
    if res == None:
        return 0
    return res

# ----------------------------------------------

def modifierGroupe(groupe, listePersonne, nom):
    """
    modifie la liste des Personnes dans un groupe
    """
    groupe.nom = nom
    for association in AssociationPersonneGroupe.query.filter_by(id_groupe=groupe.id).all():
        DB.session.delete(association)
    for idPers in listePersonne:
        DB.session.add(AssociationPersonneGroupe(id_personne=int(idPers), id_groupe=groupe.id))
    DB.session.commit()

# ----------------------------------------------

def supprimerGroupe(numGroupe):
    """
    Supprime le groupe et les associations d'un groupe identifié par numGroupe
    """
    for association in AssociationPersonneGroupe.query.filter_by(id_groupe=numGroupe).all():
        DB.session.delete(association)
    for association in AssociationGroupeSalle.query.filter_by(id_groupe=numGroupe).all():
        DB.session.delete(association)
    for association in AssociationGroupeZone.query.filter_by(id_groupe=numGroupe).all():
        DB.session.delete(association)
    for association in AssociationGroupeIndice.query.filter_by(id_groupe=numGroupe).all():
        DB.session.delete(association)

    DB.session.delete(getGroupeFromId(numGroupe))
    DB.session.commit()

# ----------------------------------------------

def nouveauGroupe(listePers, nom):
    """
    Créé un nouveau groupe avec toutes les personnes de listePers
    """
    groupe = Groupe(id=getMaxIdGroupe()+1, nom=nom)
    DB.session.add(groupe)

    for idPers in listePers:
        DB.session.add(AssociationPersonneGroupe(id=getMaxIdAssociationPersonneGroupe()+1, id_groupe=groupe.id, id_personne=int(idPers)))
    DB.session.commit()

############################ IMAGES ############################

def imageExiste(url):
    if (Image.query.filter(Image.image == url).filter(Image.indice_speciale == None).filter(Image.enigme_speciale == None).all()):
        return True

def getImageFromId(idImage):
    """
    Retourne l'image(d'escape game) identofiée par idImage
    """
    return Image.query.filter_by(id = idImage).first()

def getMaxIdImage():
    """
    Retourne l'identifiant maximum enregistré pour les instances dimages d'escape game(modèle)
    """
    res = DB.session.query(DB.func.max(Image.id).label("max")).one().max
    if res == None:
        return 0
    return res

# ----------------------------------------------

def supprimerImage(idImage):
    """
    Supprime l'image (de modele d'escape) identifiée par idImage
    """
    DB.session.delete(getImageFromId(idImage))
    DB.session.commit()

# ----------------------------------------------

def nouvelleImageReturnId(lien, idForce=None):
    """
    Créé une nouvelle image d'escape game ayant comme nom : lien
    Le idForce sert à forcer l'identifiant qui seras donné à l'image (utilisé du remplacement de l'image d'un escape)
    """
    if(idForce != None):
        image = Image(id=idForce, image=lien)
    else:
        image = Image(id=getMaxIdImage()+1, image=lien)
    DB.session.add(image)
    DB.session.commit()
    return image.id

def delete_image(id_image):
    """
    Supprime une image avec son id
    """
    image = Image.query.get(id_image)
    DB.session.delete(image)
    DB.session.commit()

def delete_images(images):
    """
    Supprime une liste d'images
    """
    for image in images:
        DB.session.delete(image)
    DB.session.commit()

def getImagePourEnigmeSpeciale():
    """
    Récupère les images (disponible) à utiliser dans les enigmes
    """
    return Image.query.filter(Image.indice_speciale == None).filter(Image.enigme_speciale == None).filter(Image.pour_enigme_speciale == True).all()

############################ INDICES ############################

def getListeModeleIndiceFromSalle(idSalle):
    """
    Retourne la liste des indices(modèle) qui seront placés aléatoirement dans les zones de la salle(modèle) identifiée par idSalle
    """
    res = []
    for association in AssociationModeleIndiceModeleSalle.query.filter_by(id_modele_salle = idSalle).all():
        res.append(getModeleIndiceFromId(association.id_modele_indice))
    return res

def getModeleIndiceFromId(idIndice):
    """
    Retourne l'indice(modèle) identifié par idIndice
    """
    return ModeleIndice.query.filter_by(id = idIndice).first()

def getMaxIdModeleIndice():
    """
    Retourne l'identifiant maximum enregistré pour les modeles d'indices
    """
    res = DB.session.query(DB.func.max(ModeleIndice.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdInstancesIndices():
    """
    Retourne l'identifiant maximum enregistré pour les instances d'indices
    """
    res = DB.session.query(DB.func.max(Indice.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdAssociationIndiceEnigme():
    """
    Retourne l'identifiant maximum enregistré pour l'association IndiceEnigme
    """
    res = DB.session.query(DB.func.max(AssociationIndiceEnigme.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdAssociationGroupeIndice():
    """
    Retourne l'identifiant maximum enregistré pour l'association AssociationGroupeIndice
    """
    res = DB.session.query(DB.func.max(AssociationGroupeIndice.id).label("max")).one().max
    if res == None:
        return 0
    return res

# ----------------------------------------------

def modifierIndice(idIndice, contenu, idModeleEnigme, image):
    """
    Modifie le contenu de l'indice (modèle) identifié par idIndice
    """
    modeleIndice = getModeleIndiceFromId(idIndice)
    modeleIndice.contenu = contenu
    modeleIndice.id_modele_enigme = idModeleEnigme
    if(image != ""):
        if(modeleIndice.image):
            modeleIndice.image.image = image
        else:
            img = Image(id=getMaxIdImage()+1, image=image)
            DB.session.add(img)
            modeleIndice.id_image = img.id

    modeleSalle = modeleIndice.modele_salles[0]
    if(modeleSalle.modele_enigmes):
        if(modeleSalle.modele_enigmes[0].enigmes):
            for instanceEnigme in modeleSalle.modele_enigmes[0].enigmes:
                if(instanceEnigme.indices):
                    for instanceIndice in instanceEnigme.indices:
                        for enigme in Enigme.query.filter_by(id_modele_enigme=idModeleEnigme).all():
                            if(enigme.groupes[0].id == instanceIndice.groupes[0].id):
                                association = AssociationIndiceEnigme.query.filter_by(id_indice = instanceIndice.id).first()
                                association.id_enigme = enigme.id
    DB.session.commit()

def decouvrirIndice(idIndice, est_enigme_speciale):
    """
    Définit l'indice identifiée par idIndice comme trouvée (découverte)
    """
    if(est_enigme_speciale == 1):
        instance = IndiceSpeciale.query.filter_by(id=idIndice).first()
    else:
        instance = Indice.query.filter_by(id=idIndice).first()
    instance.est_trouve = 1
    DB.session.commit()

# ----------------------------------------------

def supprimerModeleIndice(idIndice):
    """
    Supprime l'indice(modèle) identifié par idIndice
    """
    DB.session.delete(AssociationModeleIndiceModeleSalle.query.filter_by(id_modele_indice=idIndice).first())
    modeleIndice = getModeleIndiceFromId(idIndice)
    if(modeleIndice.image):
        DB.session.delete(modeleIndice.image)
    if(modeleIndice.indices):
        for instanceIndice in modeleIndice.indices:
            for association in AssociationIndiceEnigme.query.filter_by(id_indice = instanceIndice.id).all():
                DB.session.delete(association)
            DB.session.delete(AssociationGroupeIndice.query.filter_by(id_indice = instanceIndice.id).first())
            DB.session.delete(instanceIndice)
    DB.session.delete(modeleIndice)
    DB.session.commit()

# ----------------------------------------------

def ajouterIndice(idSalle, contenu, idModeleEnigme, image):
    """
    Ajoute un nouvel indice (modele) et l'associe à la salle(modele) identifiée par idSalle
    """
    if(image != ""):
        img = Image(id=getMaxIdImage()+1, image=image)
        DB.session.add(img)
        modeleIndice = ModeleIndice(id=getMaxIdModeleIndice()+1, contenu=contenu, id_modele_enigme=idModeleEnigme, id_image=img.id)
    else:
        modeleIndice = ModeleIndice(id=getMaxIdModeleIndice()+1, contenu=contenu, id_modele_enigme=idModeleEnigme)

    association = AssociationModeleIndiceModeleSalle(id=getMaxIdAssociationModeleindiceModeleSalle()+1, id_modele_indice=modeleIndice.id, id_modele_salle=idSalle)
    DB.session.add(modeleIndice)
    DB.session.add(association)

    enigme = Enigme.query.filter_by(id_modele_enigme = idModeleEnigme).first()
    if(enigme == None):
        enigme = Enigme(id=getMaxIdEnigme()+1, id_modele_enigme=idModeleEnigme, est_resolu=False)
        DB.session.add(enigme)


    listeInstancesSalle = Salle.query.filter_by(id_modele_salle = idSalle).all()
    if(listeInstancesSalle != []):
        for salle in listeInstancesSalle:
            indice = Indice(id=getMaxIdInstancesIndices()+1, id_modele_indice=modeleIndice.id, est_trouve=False) #la zone dans laquelle est placé cet indice est déterminée dans une fonction appelée plus tard
            DB.session.add(indice)

            association_groupe_indice = AssociationGroupeIndice(id=getMaxIdAssociationGroupeIndice()+1, id_groupe=salle.groupes[0].id, id_indice=indice.id)
            DB.session.add(association_groupe_indice)

            association_indice_enigme = AssociationIndiceEnigme(id=getMaxIdAssociationIndiceEnigme()+1 ,id_indice=indice.id, id_enigme=enigme.id)
            DB.session.add(association_indice_enigme)

            association_enigme_groupe = AssociationEnigmeGroupe(id=getMaxIdAssociationEnigmeGroupe()+1, id_enigme=enigme.id, id_groupe=salle.groupes[0].id)
            DB.session.add(association_enigme_groupe)

    DB.session.commit()


############################ SALLES ############################
def getInstanceSallesFromIdInstanceEscape(idInstance):
    """
    Retourne les instances de salles d'un escapeGame
    """
    res = getInstanceEscapeFromId(idInstance).salles
    if res == None:
        return 0
    return res

def getMaxIdInstancesSalle():
    """
    Retourne l'identifiant maximum enregistré pour les instances de salles
    """
    res = DB.session.query(DB.func.max(Salle.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdModeleSalle():
    """
    Retourne l'identifiant maximum enregistré pour les modèles de salles
    """
    res = DB.session.query(DB.func.max(ModeleSalle.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getModeleSalleFromId(idSalle):
    """
    Retourne le modèle de salle identifiée par idSalle
    """
    return ModeleSalle.query.get_or_404(idSalle)

def getListeIndicesFomSalle(idSalle):
    """
    Retourne toutes les instances d'indices qui sont dans une instance de salle identifiée par idSalle
    """
    salle = Salle.query.filter_by(id = idSalle).first()
    listeIndicesDeGroupe = AssociationGroupeIndice.query.filter_by(id_groupe = salle.groupes[0].id).all()
    listeModelesIndicesDeSalle = getListeModeleIndiceFromSalle(salle.modele_salle.id)
    res = []
    if(listeIndicesDeGroupe != []):
        for association in listeIndicesDeGroupe:
            indice = Indice.query.filter_by(id = association.id_indice).first()
            if(indice.modele_indice in listeModelesIndicesDeSalle):
                res.append(indice)
    return res

def getAssociationGroupeSalleFromIdSalle(idSalle):
    """
    Retourne l'association ayant pour salle l'instance identifiée par idSalle
    """
    return AssociationGroupeSalle.query.filter_by(id_salle = idSalle).first()
# ----------------------------------------------

def supprimerModeleSalle(idSalle, idModeleEscape):
    """
    Supprime le modèle de salle identifié par idSalle et les associations entre les indices spéciales et l'escape
    """
    salle = getModeleSalleFromId(idSalle)
    if(salle != None):

        #Suppression de l'image
        if(salle.id_image):
            DB.session.delete(Image.query.filter_by(id=salle.id_image).first())

        # REORDONNAGE DES AUTRES SALLES
        listeSalles = getListeSallesFromEscapeId(idModeleEscape)
        if(listeSalles != []):
            if(salle.ordre_salle < len(listeSalles)):
                for s in ModeleSalle.query.filter(ModeleSalle.id_modele_escape == idModeleEscape, ModeleSalle.ordre_salle > salle.ordre_salle).all():
                    s.ordre_salle -= 1
                    if(s.ordre_salle == 1):
                        s.est_libre = 1

        # INDICES, associations, modele et instances
        if(salle.modele_indices):
            for modeleIndice in salle.modele_indices:
                if(modeleIndice.image):
                    DB.session.delete(modeleIndice.image)
                if(modeleIndice.indices):
                    for instanceIndice in modeleIndice.indices:
                        DB.session.delete(AssociationGroupeIndice.query.filter_by(id_indice = instanceIndice.id).first())
                        DB.session.delete(AssociationIndiceEnigme.query.filter_by(id_indice = instanceIndice.id).first())
                        DB.session.delete(instanceIndice)

                DB.session.delete(AssociationModeleIndiceModeleSalle.query.filter_by(id_modele_indice=modeleIndice.id).first())
                DB.session.delete(modeleIndice)

        # ENIGMES, association, modele et instances
        if(salle.modele_enigmes):
            if(salle.modele_enigmes[0].image):
                DB.session.delete(salle.modele_enigmes[0].image)

            if(salle.modele_enigmes[0].enigmes):
                for instanceEnigme in salle.modele_enigmes[0].enigmes:
                    if(instanceEnigme.groupes):
                        DB.session.delete(AssociationEnigmeGroupe.query.filter(AssociationEnigmeGroupe.id_groupe == instanceEnigme.groupes[0].id, AssociationEnigmeGroupe.id_enigme == instanceEnigme.id).first())
                    DB.session.delete(instanceEnigme)

            DB.session.delete(getAssociationModeleEnigmeModeleSalleFromSalleId(idSalle))
            DB.session.delete(salle.modele_enigmes[0])

        # ZONES et instances et association
        supprimerZonesSalle(idSalle) #les vérifications de liste vide sont dedans

        # Instances Salle
        if(salle.salles):
            supprimerInstancesSalles(salle.id) #les vérifications de liste vide sont dedans

        # GROUPES et liens
        global message
        message = "La salle à bien été suprimée"
        if(len(getListeSallesFromEscapeId(idModeleEscape))-1 == 0):
            message = "La salle et les versions de celle-ci ont bien été suprimées\nN'ayant plus aucune salle jouable, tous les groupes participants à cet escape game ont été retirés"
            listeInstancesEscape = getListeInstancesFromEscapeId(idModeleEscape)
            if(listeInstancesEscape != []):
                for instanceEscape in listeInstancesEscape:
                    supprimerInstanceEscapeFromId(instanceEscape.id)

        DB.session.delete(salle)
        DB.session.commit()
        return message

# def supprimerInstancesSalles(idModeleSalle):
#     """
#     supprimer toutes les instances du modèle de salle identifié par idModeleSalle
#     """
#     listeInstancesSalles = Salle.query.filter_by(id_modele_salle = idModeleSalle).all()
#     if(listeInstancesSalles != []):
#         for instance in listeInstancesSalles:
#             if(instance.enigme):
#                 supprimerInstanceEnigme(instance.id_enigme)
#             if(instance.enigmes_speciales):
#                 enigmeSpe = instance.enigmes_speciales[0]
#                 enigmeSpe.nb_points_groupe_courant = enigmeSpe.nb_points
#                 enigmeSpe.est_resolu_groupe_courant = False
#                 DB.session.delete(AssociationEnigmeSpecialeSalle.query.filter(AssociationEnigmeSpecialeSalle.id_salle==instance.id, AssociationEnigmeSpecialeSalle.id_enigme_speciale==enigmeSpe.id).first())
#                 DB.session.delete(AssociationGroupeEnigmeSpeciale.query.filter(AssociationGroupeEnigmeSpeciale.id_enigme_speciale==enigmeSpe.id, AssociationGroupeEnigmeSpeciale.id_groupe==instance.groupes[0].id).first())
#                 if(enigmeSpe.indices_speciales):
#                     for indiceSpe in enigmeSpe.indices_speciales:
#                         DB.session.delete(AssociationGroupeIndiceSpeciale.query.filter(AssociationGroupeIndiceSpeciale.id_indice_speciale==indiceSpe.id, AssociationGroupeIndiceSpeciale.id_groupe==instance.groupes[0].id).first())
#
#             supprimerAssociationGroupeSalle(instance.id)
#             DB.session.delete(instance)
#         DB.session.commit()

# ----------------------------------------------

def ajouterModeleSalleEscapeReturnId(idEscape, numeroOrdre):
    """
    Ajoute un modèle de salle à l'escape (modèle) identifié par idEscape
    Ajuste l'ordre d'apparition de la salle avec numeroOrdre
    Met à jour les instances d'escapes associé au modèle d'escape visé par idEscape (s'il y en a)
    """
    modeleSalle = ModeleSalle(id=getMaxIdModeleSalle()+1, desc="", ordre_salle=numeroOrdre, est_personnalisee=True, est_libre=False, id_image=None, id_modele_escape=idEscape, utilise_enigme_spe=0)
    DB.session.add(modeleSalle)

    listeGroupesEscape = getListeInstancesAvecGroupesFromEscapeId(idEscape)
    if(listeGroupesEscape != []):
        for instanceEscape, groupe, etudiants in listeGroupesEscape:
            instanceSalle = Salle(id=getMaxIdInstancesSalle()+1, id_escape_game=instanceEscape.id, id_enigme=None, id_modele_salle=modeleSalle.id) #Création d'une instance de cette salle
            DB.session.add(instanceSalle)

            associationGroupeSalle = AssociationGroupeSalle(id=getMaxIdAssociationGroupeSalle()+1, est_decouverte=False, id_groupe=groupe.id, id_salle=instanceSalle.id) #Association du groupe à cette instance de salle
            DB.session.add(associationGroupeSalle)
    DB.session.commit()
    return modeleSalle.id

# ----------------------------------------------

def modifierModeleSalle(idSalle, description, estLibre, estPersonnalisee, imageFond):
    """
    Modifie les informations d'un modèle de salle identifié par idSalle
    """
    modeleSalle = getModeleSalleFromId(idSalle)
    modeleSalle.desc = description
    modeleSalle.est_libre = estLibre
    modeleSalle.est_personnalisee = estPersonnalisee
    if(imageFond != ""):
        if(modeleSalle.id_image != None):
            supprimerImage(modeleSalle.id_image)
            modeleSalle.id_image = nouvelleImageReturnId(imageFond, modeleSalle.id_image)
        else:
            modeleSalle.id_image = nouvelleImageReturnId(imageFond)
    DB.session.commit()

def modifierModeUtilisation(idModeleSalle):
    """
    Change le mode de l'énigme pour la salle -> pré-enregistré ou personnalisé
    """
    salle = getModeleSalleFromId(idModeleSalle)
    if(salle.utilise_enigme_spe == 1):
        salle.utilise_enigme_spe = 0
    else:
        salle.utilise_enigme_spe =1
    DB.session.commit()

############################ ASSOCIATION GROUPE SALLE ############################
def get_all_AssociationGroupeSalle_from_groupe(idGroupe):
    """
    Retourne l'identifiant maximum enregistré pour les associations Groupe Salle
    """
    res = AssociationGroupeSalle.query.filter_by(id_groupe = idGroupe).all()
    if res == None:
        return 0
    return res

def getMaxIdAssociationGroupeSalle():
    """
    Retourne l'identifiant maximum enregistré pour les associations Groupe Salle
    """
    res = DB.session.query(DB.func.max(AssociationGroupeSalle.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdAssociationGroupeZone():
    """
    Retourne l'identifiant maximum enregistré pour les associations Groupe Salle
    """
    res = DB.session.query(DB.func.max(AssociationGroupeZone.id).label("max")).one().max
    if res == None:
        return 0
    return res

# ----------------------------------------------

def supprimerAssociationGroupeSalle(idInstanceSalle):
    """
    Supprime les associations de groupes à l'instance de salle identifiée par idInstanceSalle
    """
    for association in AssociationGroupeSalle.query.filter_by(id_salle = idInstanceSalle).all():
        DB.session.delete(association)
    DB.session.commit()

def permettreAccesSalle(idSalle):
    """
    Met à jour l'association groupe salle en tant que découverte pour permettre aux étudiant d'y accéder
    """
    if(idSalle):
        association = AssociationGroupeSalle.query.filter_by(id_salle = idSalle).first()
        association.est_decouverte = 1
        DB.session.commit()

############################ ASSOCIATION MODELE INDICE MODELE SALLE ############################
def getMaxIdAssociationModeleindiceModeleSalle():
    """
    Retourne l'identifiant maximum enregistré pour les associations modele indice modele salle
    """
    res = DB.session.query(DB.func.max(AssociationModeleIndiceModeleSalle.id).label("max")).one().max
    if res == None:
        return 0
    return res

############################ THEMES ############################
def getListeThemes():
    """
    Retourne la liste des thèmes de la plateforme
    """
    return Theme.query.all()

def nouveauTheme(nomTheme):
    """
    Créer le nouveau thème
    """
    theme = Theme(id=getMaxIdTheme()+1, nom=nomTheme)
    DB.session.add(theme)
    DB.session.commit()

############################ ASSOCIATION MODELE ENIGME MODELE SALLE ############################
def getAssociationModeleEnigmeModeleSalleFromSalleId(idSalle):
    """
    Retourne l'association modeleSalle ModeleEnigme reliée à la salle identifié par idSalle
    """
    return AssociationModeleEnigmeModeleSalle.query.filter_by(id_modele_salle=idSalle).first()

def getMaxIdAssociationModeleEnigmeModeleSalle():
    """
    Retourne l'identifiant maximum enregistré pour les associations modele enigme modele salle
    """
    res = DB.session.query(DB.func.max(AssociationModeleEnigmeModeleSalle.id).label("max")).one().max
    if res == None:
        return 0
    return res

############################ ASSOCIATION ENIGME SPECIALE SALLE ############################
def getAssociationEnigmeSpecialeSalleFromSalleId(idSalle):
    """
    Retourne l'association Salle EnigmeSpeciale reliée à la salle identifié par idSalle
    """
    return AssociationEnigmeSpecialeSalle.query.filter_by(id_salle=idSalle).first()

def getMaxIdAssociationEnigmeSpecialeSalle():
    """
    Retourne l'identifiant maximum enregistré pour les associations modele enigme modele salle
    """
    res = DB.session.query(DB.func.max(AssociationEnigmeSpecialeSalle.id).label("max")).one().max
    if res == None:
        return 0
    return res


############################ ENIGMES ############################

def getEnigmeSalleFromModeleSalleId(idSalle):
    """
    Retourne l'énigme(modèle) associée à la salle identifiée par idSalle
    """
    association = getAssociationModeleEnigmeModeleSalleFromSalleId(idSalle)
    if(association == None):
        return None
    return getModeleEnigmeFromId(association.id_modele_enigme)

def getModeleEnigmeFromId(idEnigme):
    """
    Retourne le modèle d'énigme identifié par idEnigme
    """
    return ModeleEnigme.query.filter_by(id=idEnigme).first()

def getAllEnigme():
    """
    retourne toutes les énigmes(instances) de la plateforme
    """
    return Enigme.query.all()

def getMaxIdModeleEnigme():
    """
    Retourne l'identifiant maximum enregistré pour les associations modele Enigme
    """
    res = DB.session.query(DB.func.max(ModeleEnigme.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdEnigme():
    """
    Retourne l'identifiant maximum enregistré pour les associations enigme (instances)
    """
    res = DB.session.query(DB.func.max(Enigme.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdAssociationEnigmeGroupe():
    """
    Retourne l'identifiant maximum enregistré pour les associations getMaxIdAssociationEnigmeGroupe
    """
    res = DB.session.query(DB.func.max(AssociationEnigmeGroupe.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getListeModeleEnigmeFromModeleEscape(idModeleEscape):
    """
    Retourne la liste de tous les modèles d'enigmes du modele d'escape identifié par idModeleEscape
    """
    modeleEscape = getModeleEscapeFromId(idModeleEscape)
    res = []
    if(modeleEscape.modele_salles):
        for modeleSalle in modeleEscape.modele_salles:
            if(modeleSalle.modele_enigmes):
                for modeleEnigme in modeleSalle.modele_enigmes:
                    res.append(modeleEnigme)
    return res

def getInstanceEnigme(idEnigme):
    """
    Retourne l'instance d'énigme visée par idEnigme
    """
    return Enigme.query.filter_by(id=idEnigme).first()

# ----------------------------------------------

def modifierEnigme(idEnigme, intitule, solution, nbPoints, image):
    """
    Modifie les informations du modèle Enigme identifié par idEnigme
    """
    enigme = getModeleEnigmeFromId(idEnigme)
    enigme.intitule = intitule
    enigme.solution = solution
    if(image != "" ):
        if(enigme.image):
            enigme.image.image = image
        else:
            img = Image(id=getMaxIdImage()+1, image=image)
            DB.session.add(img)
            enigme.id_image = img.id

    if(enigme.enigmes):
        for instance in enigme.enigmes:
            instance.nb_points = nbPoints - (enigme.nb_points-instance.nb_points)
    enigme.nb_points = nbPoints

    DB.session.commit()

def resoudreEnigme(idEnigme):
    """
    Définit l'instance d'énigme identifiée par idEngime comme trouvée
    """
    instance = getInstanceEnigme(idEnigme)
    instance.est_resolu = 1
    DB.session.commit()

def resoudreEnigmeSpeciale(idEnigme):
    """
    Définit l'instance l'instance identifiée par idEngime comme trouvée
    """
    instance = getEnigmeSpecialeFromId(idEnigme)
    instance.est_resolu_groupe_courant = True
    DB.session.commit()

def retirerPointsEnigme(idEnigme):
    """
    Retire des points sur le maximum de points obtensibles pour l'instance d'énigme identifiée par idEnigme
    """
    instance = getInstanceEnigme(idEnigme)
    if(instance.nb_points > 0):
        instance.nb_points -= 1
    DB.session.commit()

def retirerPointsEnigmeSpeciale(idEnigme):
    """
    Retire des points sur le maximum de points obtensibles pour l'énigme speciale identifiée par idEnigme
    """
    instance = getEnigmeSpecialeFromId(idEnigme)
    if(instance.nb_points_groupe_courant > 0):
        instance.nb_points_groupe_courant -= 1
    DB.session.commit()

# ----------------------------------------------

def ajouterModeleEnigme(idSalle):
    """
    Ajoute un modele d'enigme associé à la salle(modele) identifiée par idSalle
    """
    modeleEnigme = ModeleEnigme(id=getMaxIdModeleEnigme()+1, intitule="", solution="", nb_points=0)
    association_modele_enigme_modele_salle = AssociationModeleEnigmeModeleSalle(id=getMaxIdAssociationModeleEnigmeModeleSalle()+1, id_modele_enigme=modeleEnigme.id, id_modele_salle=idSalle)
    DB.session.add(association_modele_enigme_modele_salle)
    DB.session.add(modeleEnigme)
    modeleSalle = getModeleSalleFromId(idSalle)
    if(modeleSalle.salles):
        for instanceSalle in modeleSalle.salles:
            instanceEnigme = Enigme(id=getMaxIdEnigme()+1, id_modele_enigme=modeleEnigme.id, est_resolu=False, nb_points=modeleEnigme.nb_points)
            instanceSalle.id_enigme = instanceEnigme.id
            DB.session.add(instanceEnigme)
            association = AssociationEnigmeGroupe(id=getMaxIdAssociationEnigmeGroupe()+1, id_enigme=instanceEnigme.id, id_groupe=instanceSalle.groupes[0].id)
            DB.session.add(association)
    DB.session.commit()
    return modeleEnigme.id

# ----------------------------------------------

def supprimerInstanceEnigme(idInstance):
    """
    Supprime l'instance d'énigme identifiée par idInstance
    """
    instanceEnigme = getInstanceEnigme(idInstance)
    DB.session.delete(AssociationEnigmeGroupe.query.filter_by(id_enigme = instanceEnigme.id).first())
    if(instanceEnigme.indices):
        for association in AssociationIndiceEnigme.query.filter_by(id_enigme = idInstance).all():
            DB.session.delete(association)
    DB.session.delete(instanceEnigme)
    DB.session.commit()

def supprimerModeleEnigme(idModeleEnigme):
    """
    Supprime le modèle d'enigme identifié par idModeleEnigme
    """
    modeleEnigme = ModeleEnigme.query.filter_by(id=idModeleEnigme).first()
    association_modele_enigme_modele_salle = AssociationModeleEnigmeModeleSalle.query.filter_by(id_modele_enigme=idModeleEnigme).first()
    if(modeleEnigme.image):
        DB.session.delete(modeleEnigme.image)
    if(modeleEnigme.enigmes):
        for instanceEnigme in modeleEnigme.enigmes:
            associations = AssociationIndiceEnigme.query.filter_by(id_enigme=instanceEnigme.id).all()
            if(associations):
                for assoc in associations:
                    DB.session.delete(assoc)

            if(instanceEnigme.groupes):
                DB.session.delete(AssociationEnigmeGroupe.query.filter(AssociationEnigmeGroupe.id_groupe == instanceEnigme.groupes[0].id, AssociationEnigmeGroupe.id_enigme == instanceEnigme.id).first())
            DB.session.delete(instanceEnigme)
    DB.session.delete(association_modele_enigme_modele_salle)
    DB.session.delete(modeleEnigme)
    DB.session.commit()

############################ ZONES ############################
def getMaxIdModeleZone():
    """
    Retourne l'identifiant maximum enregistré dans la table ModelZone
    """
    res = DB.session.query(DB.func.max(ModeleZone.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdZone():
    """
    Retourne l'identifiant maximum enregistré dans la table zone(instances)
    """
    res = DB.session.query(DB.func.max(Zone.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getListeZonesFromModeleSalle(idSalle):
    """
    Retourne la liste des zones(modèle) liées au ModeleSalle identifié par idSalle
    """
    return ModeleZone.query.filter_by(id_modele_salle = idSalle).all()

def getListeIdZonesFromModeleSalle(idSalle):
    """
    Retourne la liste des identifiants des zones(modèle) liées au ModeleSalle identifié par idSalle
    """
    return ModeleZone.query(ModeleSalle.id).filter_by(id_modele_salle = idSalle).all()

# ----------------------------------------------

def associerZonesModeleSalle(idSalle, listeZones):
    """
    Associe pour le modèle de salle identifié par idSalle, les zones contenues dans listeZones
    """
    listeInstancesSalle = Salle.query.filter_by(id_modele_salle = idSalle).all()
    
    listeModeleZonesDisponibles = []
    for zone in listeZones:
        modeleZone = ModeleZone(id=getMaxIdModeleZone()+1, pos_x=float(zone[0]), pos_y=float(zone[1]), hauteur=float(zone[2]), largeur=float(zone[3]), id_modele_salle=idSalle)
        DB.session.add(modeleZone)
        listeModeleZonesDisponibles.append(modeleZone)

    if(listeInstancesSalle != []):
        for versionSalle in listeInstancesSalle:

            listeIndicesDansInstanceSalle = getListeIndicesFomSalle(versionSalle.id)

            while listeModeleZonesDisponibles != []:
                modeleZone = random.choice(listeModeleZonesDisponibles)
                if(listeIndicesDansInstanceSalle != []):
                    instanceIndice = random.choice(listeIndicesDansInstanceSalle)
                    zone = Zone(id=getMaxIdZone()+1, contient_indice=True, id_salle=versionSalle.id, id_modele_zone=modeleZone.id)

                    instanceIndice.id_zone = zone.id

                    listeIndicesDansInstanceSalle.remove(instanceIndice)
                    DB.session.add(zone)
                else:
                    zone = Zone(id=getMaxIdZone()+1, contient_indice=False, id_salle=versionSalle.id, id_modele_zone=modeleZone.id)
                    DB.session.add(zone)

                listeModeleZonesDisponibles.remove(modeleZone)
                association_groupe_zone = AssociationGroupeZone(id_zone=zone.id, id_groupe=versionSalle.groupes[0].id)
                DB.session.add(association_groupe_zone)

    DB.session.commit()

def associerZonesModeleSalleSansChangerZones(idSalle):
    """
    Associe pour le modèle de salle identifié par idSalle, les zones déjà enregistrées
    """
    listeInstancesSalle = Salle.query.filter_by(id_modele_salle = idSalle).all()
    if(listeInstancesSalle != []):
        for versionSalle in listeInstancesSalle:
            listeZonesDisponibles = versionSalle.zones
            copieListezonesDisponibles = listeZonesDisponibles.copy()
            listeIndicesDansInstanceSalle = getListeIndicesFomSalle(versionSalle.id)
            while copieListezonesDisponibles != []:
                zone = random.choice(copieListezonesDisponibles)

                if(listeIndicesDansInstanceSalle != []):
                    instanceIndice = random.choice(listeIndicesDansInstanceSalle)

                    zone.contient_indice = True
                    instanceIndice.id_zone = zone.id

                    listeIndicesDansInstanceSalle.remove(instanceIndice)
                else:
                    zone.contient_indice = False
                copieListezonesDisponibles.remove(zone)

    DB.session.commit()

# ----------------------------------------------

def supprimerZonesSalle(idSalle):
    """
    Supprime les zones associé au ModeleSalle identifié par idSalle
    supprime aussi les instances de zones
    """
    listeZones = getListeZonesFromModeleSalle(idSalle)
    if(listeZones != []):
        for modeleZone in listeZones:
            if(modeleZone.zones):
                for instanceZone in modeleZone.zones:
                    if(instanceZone.indices_speciales):
                        association = AssociationZoneIndiceSpeciale.query.filter(AssociationZoneIndiceSpeciale.id_indice_speciale == instanceZone.indices_speciales[0].id, AssociationZoneIndiceSpeciale.id_zone == instanceZone.id).first()
                        DB.session.delete(association)
                        DB.session.commit()
                    if(instanceZone.groupes):
                        association2 = AssociationGroupeZone.query.filter(AssociationGroupeZone.id_groupe == instanceZone.groupes[0].id, AssociationGroupeZone.id_zone == instanceZone.id).first()
                        DB.session.delete(association2)
                        DB.session.commit()
                    if(instanceZone.indices):
                        instanceZone.indices[0].id_zone = None
                        DB.session.commit()
                    DB.session.delete(instanceZone)
            DB.session.delete(modeleZone)
        DB.session.commit()

############################ ACTIONS SUR LA BD ############################
def load_personne_by_email(email_voulu):
    """
    Retourne la personne visée par email identifié par email_voulu
    """
    return DB.session.query(Personne).filter_by(email=email_voulu).first()

@login_manager.user_loader
def load_personne_by_id(id_voulu):
    """
    Retourne la personne identifiée par id_voulu
    """
    return DB.session.query(Personne).filter_by(id=id_voulu).first()

def edit_id_user(actuel_id, nouveau_id):
    return Personne.query.filter(Personne.id==actuel_id).update({Personne.id: nouveau_id})


def checkImageIdentique(image, file):
    """
    Vérifie si l'image passé en param est identique à celle stocké dans les fichier
    """
    pass

def importerImagesEnigmesSpeciales(images):
    for url in images:
        image = Image(image=url, pour_enigme_speciale=True)
        DB.session.add(image)
    DB.session.commit()


############################ STATISTIQUES ############################


def getNbSallesTotal(idEscape):
    """
    retourne le nb de salles total d'un escape game d'un groupe
    """
    return len(getListeSallesFromEscapeId(idEscape))

def getNbSallesTermine(idEscape,idGroupe):
    """
    retourne le nb de salles terminées d'un escape game par un groupe
    """
    res = 0
    sallesInstanceEscape = getInstanceSallesFromIdInstanceEscape(idEscape)
    sallesGroupe = get_all_AssociationGroupeSalle_from_groupe(idGroupe)
    for sallesInstanceEscape in sallesInstanceEscape:
        for salleGroupe in sallesGroupe:
            if sallesInstanceEscape.id == salleGroupe.id_salle:
                modeleSalle=getModeleSalleFromId(sallesInstanceEscape.id_modele_salle)
                if salleGroupe.est_decouverte or modeleSalle.est_libre:
                    res+=1
    return res

def tauxDeProgression(idescape,idgroupe):
    nbSallesTotal = getNbSallesTotal(idescape)
    nbSallesTermine = getNbSallesTermine(idescape,idgroupe)
    return (nbSallesTermine / nbSallesTotal)*100

def getNbPointsGroupe(idEscape):
    """
    retourne le nb de points obtenus par un groupe sur cet escape
    """
    return getNoteFromEscape(idEscape)[0]

def getNbPointsMaxGroupe(idEscape):
    """
    retourne le nb de points maximale que peut obtenir un groupe sur cet escape
    """
    return getNoteFromEscape(idEscape)[2]

def getValeur(idEscape):
    """
    retourne le nb de points maximale que peut obtenir un groupe sur cet escape
    """
    a = getNbPointsGroupe(idEscape)
    b = getNbPointsMaxGroupe(idEscape)
    valeur = ((a / b)*100)//1
    return valeur

############################ ENIGMES SPECIALES ############################

def getEnigmeSpecialeFromSalleId(idSalle):
    """
    Retourne l'énigme spéciale associée à la salle identifiée par idSalle
    """
    association = getAssociationEnigmeSpecialeSalleFromSalleId(idSalle)
    if(association == None):
        return None
    return getEnigmeSpecialeFromId(association.id_enigme_speciale)

def ajouterAssociationEnigmeSpecialSalle(idEnigmeSpeciale, idSalle):
    """
    Ajoute une association avec l'id de EnigmeSpeciale et idSalle
    """
    association = AssociationEnigmeSpecialeSalle(id_enigme_speciale = idEnigmeSpeciale, id_salle = idSalle)
    DB.session.add(association)
    DB.session.commit()
    return True

def getEnigmeSpecialeFromId(idEnigmeSpeciale):
    """
    Retourne l'énigme spéciale identifié par idEnigmeSpeciale
    """
    return EnigmeSpeciale.query.filter_by(id=idEnigmeSpeciale).first()

def getAllEnigmeSpeciale():
    """
    retourne toutes les énigmes spéciales
    """
    return EnigmeSpeciale.query.all()

def getMaxIdEnigmeSpeciale():
    """
    Retourne l'identifiant maximum enregistré pour les associations enigme speciale
    """
    res = DB.session.query(DB.func.max(EnigmeSpeciale.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdAssociationEnigmeSpecialeGroupe():
    """
    Retourne l'identifiant maximum enregistré pour les associations getMaxIdAssociationEnigmeSpecialeGroupe
    """
    res = DB.session.query(DB.func.max(AssociationGroupeEnigmeSpeciale.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdAssociationGroupeEnigmeSpeciale():
    """
    Retourne l'identifiant maximum enregistré pour les associations getMaxIdAssociationEnigmeSpecialeGroupe
    """
    res = DB.session.query(DB.func.max(AssociationGroupeEnigmeSpeciale.id).label("max")).one().max
    if res == None:
        return 0
    return res

def ajouterAssociationGroupeEnigmeSpeciale(idEnigmeSpeciale, idGroupe):
    """
    Ajoute l'association entre l'énigme Speciale et le groupe
    """
    association = AssociationGroupeEnigmeSpeciale(id_enigme_speciale= idEnigmeSpeciale, id_groupe= idGroupe)
    DB.session.add(association)
    DB.session.commit()
    return True

# ----------------------------------------------

def supprimerEnigmeSpeciale(idEnigmeSpeciale):
    """
    Supprime l'énigme spéciale identifiée par idEnigmeSpeciale
    """
    enigmeSpe = getEnigmeSpecialeFromId(idEnigmeSpeciale)
    if(enigmeSpe.indices_speciales):
        for indiceSpe in enigmeSpe.indices_speciales:
            if(indiceSpe.zones):
                for zone in indiceSpe.zones:
                    DB.session.delete(AssociationZoneIndiceSpeciale.query.filter(AssociationZoneIndiceSpeciale.id_indice_speciale == indiceSpe.id, AssociationZoneIndiceSpeciale.id_zone == zone.id).first())
            DB.session.delete(indiceSpe)
    association = AssociationEnigmeSpecialeSalle.query.filter_by(id_enigme_speciale=idEnigmeSpeciale).first()
    association2 = AssociationGroupeEnigmeSpeciale.query.filter_by(id_enigme_speciale=idEnigmeSpeciale).all()
    if(association):
        DB.session.delete(association)
    if(association2):
        for asso in association2:
            DB.session.delete(asso)
    DB.session.delete(enigmeSpe)
    DB.session.commit()

def supprimerToutesEnigmeSpeciale():
    """
    Supprime toutes les énigmes spéciales
    """
    for enigmeSpe in EnigmeSpeciale.query.all():
        supprimerEnigmeSpeciale(enigmeSpe.id)

def supprimerAssociationEnigmeSpeSalleGroupe(idEnigmeSpe):
    """
    Supprime l'association entre une énigme spe et une salle (instance) (elle même associée à un groupe)
    """
    DB.session.delete(AssociationEnigmeSpecialeSalle.query.filter_by(id_enigme_speciale=idEnigmeSpe).first())
    DB.session.delete(AssociationGroupeEnigmeSpeciale.query.filter_by(id_enigme_speciale=idEnigmeSpe).first())
    enigmeSpe = getEnigmeSpecialeFromId(idEnigmeSpe)
    enigmeSpe.nb_points_groupe_courant = enigmeSpe.nb_points
    enigmeSpe.est_resolu_groupe_courant = False
    if(enigmeSpe.indices_speciales):
        for indice in enigmeSpe.indices_speciales:
            indice.est_trouve = False
            if(indice.zones):
                DB.session.delete(AssociationZoneIndiceSpeciale.query.filter_by(id_indice_speciale=indice.id).first())
                DB.session.delete(AssociationGroupeIndiceSpeciale.query.filter_by(id_indice_speciale=indice.id).first())
    DB.session.commit()

# ----------------------------------------------
def associerIndicesEnigmeSpeciale(idSalle, idEnigmeSpe, idGroupe):
    """
    Associe une tous les indices d'une énigme spéciale à une zone et à une groupe (idSalle->modele)
    """
    modeleSalle = getModeleSalleFromId(idSalle)
    if(modeleSalle.salles):
        for instanceSalle in modeleSalle.salles:
            if(instanceSalle.groupes and instanceSalle.groupes[0].id == idGroupe):
                # On a trouvé la salle ici

                enigmeSpeciale = getEnigmeSpecialeFromId(idEnigmeSpe)

                #INDICES SPECIAUX
                listeIndicesDisponibles = []
                if(enigmeSpeciale.indices_speciales):
                    for indice_spe in enigmeSpeciale.indices_speciales:
                        listeIndicesDisponibles.append(indice_spe)
                cptZoneSansIndices = 0
                if(instanceSalle.zones):
                    for zone in instanceSalle.zones:
                        if(len(listeIndicesDisponibles) != 0):
                            randomIndiceSpe = random.choice(listeIndicesDisponibles)
                            listeIndicesDisponibles.remove(randomIndiceSpe)

                            if(randomIndiceSpe.zones):
                                association = AssociationZoneIndiceSpeciale.query.filter(AssociationZoneIndiceSpeciale.id_indice_speciale == randomIndiceSpe.id, AssociationZoneIndiceSpeciale.id_zone == randomIndiceSpe.zones[0].id).first()
                                association.id_zone = zone.id
                            else:
                                lierIndiceSpecialeZone(randomIndiceSpe.id, zone.id)

                            if(randomIndiceSpe.groupes):
                                association = AssociationGroupeIndiceSpeciale.query.filter(AssociationGroupeIndiceSpeciale.id_indice_speciale == randomIndiceSpe.id, AssociationGroupeIndiceSpeciale.id_groupe == randomIndiceSpe.groupes[0].id).first()
                            else:
                                lierIndiceSpecialeGroupe(randomIndiceSpe.id, instanceSalle.groupes[0].id)
                        else:
                            cptZoneSansIndices += 1
                DB.session.commit()
            if(cptZoneSansIndices == 0):
                if(len(listeIndicesDisponibles) == 1):
                    return ("vert", "1 énigme pré-enregistré a bien été associée. Toutes les zones ont reçu un indice. Il reste 1 indice qui n'a pas été placé.")
                return ("vert", "1 énigme pré-enregistré a bien été associée. Toutes les zones ont reçu un indice. Il reste "+str(len(listeIndicesDisponibles))+" indices qui n'ont pas été placés.")
            return ("vert", "1 énigme pré-enregistrée à bien été associée. "+str(cptZoneSansIndices)+" zones n'ont pas reçu d'indice car il n'y en avait pas assez d'enregistrés pour cette énigme pré-enregistrée.")
    return ("rouge", "Aucun groupe n'a encore été ajouté à cet escape game")

def associerEnigmeSpecialeSalle(idSalle, idDifficulte, idTheme):
    """
    Créé une association entre une salle(modele) et une énigme spéciale rescpectant les critères de sélection (thème et difficulté)
    """
    listeInstancesSalle = Salle.query.filter_by(id_modele_salle = idSalle).all()
    if(listeInstancesSalle):
        for instanceSalle in listeInstancesSalle:
            if(instanceSalle.enigmes_speciales):
                DB.session.delete(AssociationEnigmeSpecialeSalle.query.filter(AssociationEnigmeSpecialeSalle.id_enigme_speciale == instanceSalle.enigmes_speciales[0].id, AssociationEnigmeSpecialeSalle.id_salle == instanceSalle.id).first())
                DB.session.delete(AssociationGroupeEnigmeSpeciale.query.filter(AssociationGroupeEnigmeSpeciale.id_enigme_speciale == instanceSalle.enigmes_speciales[0].id, AssociationGroupeEnigmeSpeciale.id_groupe == instanceSalle.groupes[0].id).first())

    listeEnigmesDesCriteres = EnigmeSpeciale.query.filter(EnigmeSpeciale.id_difficulte == idDifficulte, EnigmeSpeciale.id_theme == idTheme).all()
    if(listeEnigmesDesCriteres):
        listeEnigmesDisponibles = []
        for enigme in listeEnigmesDesCriteres:
            if(AssociationEnigmeSpecialeSalle.query.filter_by(id_enigme_speciale=enigme.id).first() == None):
                listeEnigmesDisponibles.append(enigme)

        if(listeEnigmesDisponibles == []):
            return ("rouge", "Toutes les énigmes ayant ces critères sont déjà associées")

        if(listeInstancesSalle):
            if(len(listeEnigmesDisponibles) < len(listeInstancesSalle)):
                return ("rouge", "Il n'y a pas assez d'énigmes correspondant à vos critères pour tous les groupes associés à cet escape. Il faudrait rajouter "+str(len(listeInstancesSalle)-len(listeEnigmesDisponibles))+" énigmes.")

            for instanceSalle in listeInstancesSalle:
                # ENIGME SPECIALE
                enigmeAuHasard = random.choice(listeEnigmesDisponibles)
                listeEnigmesDisponibles.remove(enigmeAuHasard)

                DB.session.add(AssociationEnigmeSpecialeSalle(id=getMaxIdAssociationEnigmeSpecialeSalle()+1, id_enigme_speciale=enigme.id, id_salle=instanceSalle.id))
                DB.session.add(AssociationGroupeEnigmeSpeciale(id=getMaxIdAssociationGroupeEnigmeSpeciale()+1, id_enigme_speciale=enigme.id, id_groupe=instanceSalle.groupes[0].id))

                #INDICES SPECIAUX
                listeIndicesDisponibles = []
                if(enigmeAuHasard.indices_speciales):
                    for indice_spe in enigmeAuHasard.indices_speciales:
                        listeIndicesDisponibles.append(indice_spe)

                if(instanceSalle.zones):
                    cptZoneSansIndices = 0
                    for zone in instanceSalle.zones:
                        if(len(listeIndicesDisponibles) != 0):
                            randomIndiceSpe = random.choice(listeIndicesDisponibles)
                            listeIndicesDisponibles.remove(randomIndiceSpe)

                            if(randomIndiceSpe.zones):
                                association = AssociationZoneIndiceSpeciale.query.filter(AssociationZoneIndiceSpeciale.id_indice_speciale == randomIndiceSpe.id, AssociationZoneIndiceSpeciale.id_zone == randomIndiceSpe.zones[0].id).first()
                                association.id_zone = zone.id
                            else:
                                lierIndiceSpecialeZone(randomIndiceSpe.id, zone.id)

                            if(randomIndiceSpe.groupes):
                                association = AssociationGroupeIndiceSpeciale.query.filter(AssociationGroupeIndiceSpeciale.id_indice_speciale == randomIndiceSpe.id, AssociationGroupeIndiceSpeciale.id_groupe == randomIndiceSpe.groupes[0].id).first()
                            else:
                                lierIndiceSpecialeGroupe(randomIndiceSpe.id, instanceSalle.groupes[0].id)
                        else:
                            cptZoneSansIndices += 1
                DB.session.commit()

            if len(listeInstancesSalle) == 1:
                if(cptZoneSansIndices == 0):
                    if(len(listeIndicesDisponibles) == 1):
                        return ("vert", "1 énigme pré-enregistré a bien été associée. Toutes les zones ont reçu un indice. Il reste 1 indice qui n'a pas été placé.")
                    return ("vert", "1 énigme pré-enregistré a bien été associée. Toutes les zones ont reçu un indice. Il reste "+str(len(listeIndicesDisponibles))+" indices qui n'ont pas été placés.")
                return ("vert", "1 énigme pré-enregistrée à bien été associée. "+str(cptZoneSansIndices)+" zones n'ont pas reçu d'indice car il n'y en avait pas assez d'enregistrés pour cette énigme pré-enregistrée.")
            elif(cptZoneSansIndices == 0):
                if(len(listeIndicesDisponibles) == 1):
                    return ("vert", str(len(listeInstancesSalle))+" énigmes pré-enregistrées ont bien été associées. Toutes les zones ont reçu un indice. Il reste 1 indice qui n'a pas été placé.")
                return ("vert", str(len(listeInstancesSalle))+" énigmes pré-enregistrées ont bien été associées. Toutes les zones ont reçu un indice. Il reste "+str(len(listeIndicesDisponibles))+" indices qui n'ont pas été placés.")
            return ("vert", str(len(listeInstancesSalle))+" énigmes pré-enregistrées ont bien été associées. "+str(cptZoneSansIndices)+" zones n'ont pas reçu d'indice car il n'y en avait pas assez d'enregistrés pour cette énigme pré-enregistrée.")
        return ("rouge", "Aucun groupe n'a encore été ajouté à cet escape game")
    return ("rouge", "Aucune énigme correspondant à vos critères n'est encore enregistrée")

############################ INDICES SPECIALES ############################

def associerIndiceSpecialeImage(id_indice, id_image):
    indice = IndiceSpeciale.query.get(id_indice)
    indice.id_image = id_image
    DB.session.commit()

def associerEnigmeSpecialeImage(id_enigme, id_image):
    enigme = EnigmeSpeciale.query.get(id_enigme)
    enigme.id_image = id_image
    DB.session.commit()

def getListeIndiceSpecialesFromSalle(idSalle):
    """
    Retourne la liste des indices speciales qui seront placés aléatoirement dans les zones de la salle(modèle) identifiée par idSalle
    """
    res = []
    for zone in Zone.query.filter_by(id_salle = idSalle).all():
        association = AssociationZoneIndiceSpeciale.query.filter_by(id_zone=zone.id).first()
        res.append(getIndiceSpecialeFromId(association.id_indice_speciale))
    return res

def getListeIndiceSpecialeFromIdEnigmeSpe(idEnigmeSpe):
    """
    Retourne la liste des indices appartenant à l'EnigmeSpeciale   idEnigmeSpe donnée en paramètre
    """
    res = []
    enigmeSpeciale = getEnigmeSpecialeFromId(idEnigmeSpe)
    for indice in enigmeSpeciale.indices_speciales:
        res.append( indice )
    return res

def getIndiceSpecialeFromId(idIndiceSpeciale):
    """
    Retourne l'indice speciale identifié par idIndiceSpeciale
    """
    return IndiceSpeciale.query.filter_by(id = idIndiceSpeciale).first()

def getMaxIdIndiceSpeciale():
    """
    Retourne l'identifiant maximum enregistré pour les indices speciales
    """
    res = DB.session.query(DB.func.max(IndiceSpeciale.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdAssociationGroupeIndiceSpeciale():
    """
    Retourne l'identifiant maximum enregistré pour l'association AssociationGroupeIndiceSpeciale
    """
    res = DB.session.query(DB.func.max(AssociationGroupeIndiceSpeciale.id).label("max")).one().max
    if res == None:
        return 0
    return res

def getMaxIdAssociationZoneIndiceSpeciale():
    """
    Retourne l'identifiant maximum enregistré pour l'association AssociationZoneIndiceSpeciale
    """
    res = DB.session.query(DB.func.max(AssociationZoneIndiceSpeciale.id).label("max")).one().max
    if res == None:
        return 0
    return res

# ----------------------------------------------

def supprimerIndiceSpeciale(idIndiceSpeciale):
    """
    Supprime l'indice speciale identifié par idIndiceSpeciale
    """
    DB.session.delete(AssociationGroupeIndiceSpeciale.query.filter_by(id_indice_speciale=idIndiceSpeciale).first())
    DB.session.delete(AssociationZoneIndiceSpeciale.query.filter_by(id_indice_speciale=idIndiceSpeciale).first())
    DB.session.delete(getIndiceSpecialeFromId(idIndiceSpeciale))
    DB.session.commit()

# ----------------------------------------------

def ajouterIndiceSpeciale(contenu, id_enigme_speciale):
    """
    Ajoute un nouvel indice speciale et l'associe à l'EnigmeSpeciale
    """
    IndiceSpeciale = IndiceSpeciale(id=getMaxIdIndiceSpeciale()+1, contenu=contenu, id_enigme_speciale=id_enigme_speciale)
    DB.session.add(IndiceSpeciale)
    DB.session.commit()

def lierIndiceSpecialeZone(id_indice_speciale, id_zone):
    """
    Liaison de l'indice speciale a la zone
    """
    association = AssociationZoneIndiceSpeciale(id=getMaxIdAssociationZoneIndiceSpeciale()+1, id_indice_speciale=id_indice_speciale, id_zone=id_zone)
    Zone.query.filter_by(id=id_zone).first().contient_indice = True
    DB.session.add(association)
    DB.session.commit()

def lierIndiceSpecialeGroupe(id_indice_speciale, id_groupe):
    """
    Liaison de l'indice speciale au groupe
    """
    association = AssociationGroupeIndiceSpeciale(id=getMaxIdAssociationGroupeIndiceSpeciale()+1, id_indice_speciale=id_indice_speciale, id_groupe=id_groupe)
    DB.session.add(association)
    DB.session.commit()

def ajouterAssociationsIndicesSpecialesPourLeGroupe(idEnigmeSpe, idSalle, idGroupe):
    """
    Associe les indices_spéciaux d'une énigme spéciale aux zones d'une salle d'un escape d'un groupe. (ici idSalle est l'id du modèle donc il nous faut le groupe pour trouver l'instance d'escape)
    """
    enigmeSpeciale = getEnigmeSpecialeFromId(idEnigmeSpe)
    for instance in getEscapeFromGroupe(idGroupe):
        if(instance.salles):
            for salle in instance.salles:
                if(salle.id == idSalle):
                    instanceEscape = instance
                    instanceSalle = salle
                    break
        else:
            break

    listeIndicesDisponibles = []
    if(enigmeSpeciale.indices_speciales):
        for indice_spe in enigmeSpeciale.indices_speciales:
            listeIndicesDisponibles.append(indice_spe)

    if(instanceSalle.zones):
        cptZoneSansIndices = 0
        for zone in instanceSalle.zones:
            if(len(listeIndicesDisponibles) != 0):
                randomIndiceSpe = random.choice(listeIndicesDisponibles)
                listeIndicesDisponibles.remove(randomIndiceSpe)

                if(randomIndiceSpe.zones):
                    association = AssociationZoneIndiceSpeciale.query.filter(AssociationZoneIndiceSpeciale.id_indice_speciale == randomIndiceSpe.id, AssociationZoneIndiceSpeciale.id_zone == randomIndiceSpe.zones[0].id).first()
                    print(association.id)
                    association.id_zone = zone.id
                    zone.contient_indice = True
                else:
                    lierIndiceSpecialeZone(randomIndiceSpe.id, zone.id)

                if(randomIndiceSpe.groupes):
                    association = AssociationGroupeIndiceSpeciale.query.filter(AssociationGroupeIndiceSpeciale.id_indice_speciale == randomIndiceSpe.id, AssociationGroupeIndiceSpeciale.id_groupe == randomIndiceSpe.groupes[0].id).first()
                else:
                    lierIndiceSpecialeGroupe(randomIndiceSpe.id, idGroupe)
            else:
                cptZoneSansIndices += 1
        DB.session.commit()
        if(cptZoneSansIndices == 0):
            if(len(listeIndicesDisponibles) == 1):
                return "L'énigme pré-enregistrée à bien été associée. Toutes les zones ont reçu un indice. Il reste 1 indice qui n'a pas été placé."
            return "L'énigme pré-enregistrée à bien été associée. Toutes les zones ont reçu un indice. Il reste "+str(len(listeIndicesDisponibles))+" indices qui n'ont pas été placés."
        return "L'énigme pré-enregistrée à bien été associée. "+str(cptZoneSansIndices)+" zones n'ont pas reçu d'indice car il n'y en avait pas assez d'enregistrés pour cette énigme pré-enregistrée."
