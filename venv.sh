read -p "Voulez vous (re)créer le virtualenv ?[y/enter]" reponse

if echo "$reponse" | grep -iq "^y" ;then
  rm -rf venv/
  virtualenv -p python3 venv/
  source venv/bin/activate
  pip3 install -r requirements.txt
else
	source venv/bin/activate
	pip3 install -r requirements.txt
fi

cd ./mathscape

read -p "Lancer l'application [y/enter]" reponse
if echo "$reponse" | grep -iq "^y" ;then
  flask launch
  firefox localhost:5000
  flask run
fi

clear
